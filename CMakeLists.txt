cmake_minimum_required(VERSION 3.7.2)

project(libwaved)

set(CMAKE_CXX_STANDARD 20)

include_directories("${PROJECT_SOURCE_DIR}/source" "/usr/local/include")
add_subdirectory(tests)
add_subdirectory(examples)
