# libwaved

Copyright (c) 2018 Jeremy Jurksztowicz. All rights reserved.

## About
**TLDR; _Play_ and _edit_ sound files.**
libwaved ("lib-wave-ed") is a library to easily and quickly play and edit audio files; 
providing super fast, non-destructive editing over discontiguous, sample-accurate 
selections. Underneath the C11 API is a C++ 17 engine with a minimal and unencumbered 
set of classes and functions.

## Install
**TLDR; CMake C11 library or copy into a C++17 project, no external dependencies.**
libwaved is presented as a strongly specified C API, and relying only on C++ 17 standard 
libraries for it's implementation. Building is done with CMake, or the entire project folder 
can be copied into your project if you are able to turn on C++ 17 support in your toolchain.

```$user@libwaved-master: cmake```

or:

```$user@downloads: cp libwaved-master your/project/dir/libwaved```

There are no packaged audio driver facilities, but example programs are included binding 
libwaved to RtAudio, CoreAudio, SDL, Juce, etc., are included for reference. 

## Design
**TLDR; Low-level imperative C, build higher level API's on top of this.**
Libwaved is a low-level library, designed to support safer and more sophisticated 
abstractions above it. It is in the form of a classically imperative C-style API because it 
translates to any system very easily and with minimal technical trouble, and because it 
provides all the raw support necessary to build other, non-imperative API's. This library is 
the closest thing I can get to Alan Kay's "microcode", the raw primitive CPU operations
that could be programmed to support a higher levels of software abstraction.

A reference implementation of a pure functional lazy API built on top of libwaved is 
included in the examples.

## Technical Overview
**TLDR; Platform and file agnostic, independent layers, custom error handling.**

#### Platform Specific Details
Audio development tends to be littered with a _lot_ of third party libraries, many of which
bring in dependencies to platform specific details. In contrast: the only dependency of 
libwaved is a modern compiler with C++ 17 support, everything that standard C++ cannot 
provide is injected into the library by the user, through the use of C function pointers.
Notably, memory and file streams are provided by passing interfaces for the use and 
release of those resources into the respective libwaved API calls.  For example, when 
loading a file there is one function pointer to read from the file, and one _optional_ function
pointer to release the file after we are done with it:
 
##### _Example 0: Loading a raw wave_:
    int read_stream(void* stream, Range r, wv_SampleArray inp, 
    wv_SampleArray * outp) {
        FILE * file = (FILE*)stream;
        fseek(r.begin, 0, SEEK_SET);
        return fread(inp, sizeof(wv_Sample) * wv_Range_size(r));
    }
    
    void release_stream(void* stream) {
        FILE * file = (FILE*)stream;
        fclose(file);
    }
    
    // ...
    wv_Wave wave = wv_Wave_make_empty();
    FILE * raw_stream = fopen("my/file/path.raw");
    wv_Wave_set_stream(wave, raw_stream, 1, read_stream, release_stream);
    
There are many more examples using ubiquitous libraries such as libsndfile in the examples
directory.

#### Independent Layers
Libsndfile is divided into 4 main components, each built on top of the other. From the 
lowest to highest levels they are:
    1. `wv_RangeSet`:       Build and modify discontiguous integral ranges.
    2. `wv_Wave`:               Build and modify waves. Uses `wv_RangeSet` to select ranges.
    3. `wv_ReadHead`:       Copies and transforms the contents of `wv_Wave` in real-time.
    4. `wv_HeadDriver`:   Manages multiple `wv_ReadHead`'s and communication between 
                                        real-time and non-realtime contexts.
                                        
For applications that do not play waves as audio data, both the head and driver layers are
unlikely to be useful. For audio applications that want to manage their own memory and
real-time communication, then the head layer will probably be an appropriate layer of 
abstraction that doesn't take away control. For most applications, it's nice to have 
complexities hidden entirely away behind the driver. Note that the head and driver layers
were designed to support complex audio processing, so even a professional digital audio
application may find enough control in those interfaces.

#### Error Handling
By default errors are propogated as C++ exceptions, which can be turned off at either 
compile-time, or at run-time if you don't mind linking to C++ exception support.


##### _Example 1: Editing a wave_:
    int n = wv_Wave_get_frame_count(wave);
    wv_RangeSet gaps = wvRangeSet_make_empty();
    for(i = 0; i < 100; i+=2) {
        wv_Range r{wv_Pos(double(i)/double(n)), 
                   wv_Pos(double(i+1)/double(n))};
        wv_RangeSet_insert(gaps, r);
    }
    wv_Wave_remove(wave, gaps_range);
