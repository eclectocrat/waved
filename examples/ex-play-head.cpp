///
/// This examples shows the playback of Wave_list<T> using a Head<T> but not
/// a Head_processor<T>. For an example that uses the full playback API and all
/// it's features, see ex-play-heads.cpp.
///

#include <atomic>
#include <iostream>
#include <unistd.h>
#include <head.hpp>
#include "util.hpp"

std::shared_ptr<ec::Wave_list<float>> list;
std::shared_ptr<ec::Head<float>> head;
std::vector<ec::Buffer<float>> buffers;

int drive_audio(void *out, void *in, unsigned int frames, double stream_time,
                RtAudioStreamStatus status, void *data) {
    std::memset(out, 0, frames * 2 * sizeof(float));
    auto output = reinterpret_cast<float*>(out);
    auto outs = ec::Span_array<float>({output, output + frames}, 
                                      {output + frames, output + frames*2});
    if (list->channel_count() == 2) {
        head->move_and_read(outs, buffers);
    } else {
        assert(list->channel_count() == 1);
        head->move_and_read({outs[0]}, buffers);
        std::memcpy(outs[1].data(), outs[0].data(), frames * sizeof(float));
    }
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Usage: wave-play path/to/audio/file.[wav, aif, ...]\n";
        return 0;
    }
    RtAudio dac;
    try {
        auto [settings, options] = ec::util::setup_RtAudio(dac);
        auto [stream, info] = ec::util::setup_Sndfile(argv[1]);
        list = ec::util::make_mapped_list_from_stream(stream, info);
        head = std::make_shared<ec::Head<float>>();
        head->attach(list, {0, list->frame_count()}, list->frame_count());
        head->set_speed(.7);
        head->set_dir(ec::Head<float>::Direction::backward);
        buffers =
            ec::util::make_uninitialized_buffers<float>(info.channels, 4096);
        ec::util::start_RtAudio(dac, settings, options, drive_audio, nullptr);
        while (head->pos() > 0) {
            usleep(20);
        }
    } catch(std::exception const& err) {
        std::cerr << err.what() << std::endl;
        return -1;
    } catch (RtAudioError& err) {
        err.printMessage();
        return -1;
    }
    return 0;
}
