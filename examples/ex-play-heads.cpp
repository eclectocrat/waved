///
/// This examples shows the playback of Wave<T> and Wave_list<T> using multiple
/// Head<T>'s in a Head_processor<T>.
///

#include <atomic>
#include <iostream>
#include <unistd.h>
#include "util.hpp"

std::shared_ptr<ec::Wave_list<float>> list;
std::vector<std::shared_ptr<ec::Head<float>>> heads;
ec::util::Processor proc;

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Usage: wave-play path/to/audio/file.[wav, aif, ...]\n";
        return 0;
    }
    RtAudio dac;
    try {
        auto [settings, options] = ec::util::setup_RtAudio(dac);
        auto [stream, info] = ec::util::setup_Sndfile(argv[1]);
        list = ec::util::make_mapped_list_from_stream(stream, info);
        
        heads.push_back(std::make_shared<ec::Head<float>>());
        heads[0]->attach(list, {0, list->frame_count()}, 0);
        heads[0]->set_speed(.7);
        heads.push_back(std::make_shared<ec::Head<float>>());
        heads[1]->attach(list, {0, list->frame_count()}, 0);
        heads[1]->set_speed(.85);
        heads.push_back(std::make_shared<ec::Head<float>>());
        heads[2]->attach(list, {0, list->frame_count()}, 0);
        heads[2]->set_speed(1);
        
        proc.prepare_before_processing(16, 256, 4096);
        for(auto h: heads) {
            proc.send_message(
                {&proc, h, ec::util::Processor::Message::Meta::add});
        }
        ec::util::start_RtAudio(dac, settings, options,
                                ec::util::drive_head_processor_RtAudio, &proc);
        while (not heads.empty()) {
            ec::util::Processor::Message msg;
            while (proc.try_recieve_message(msg)) {
                if (msg.meta == ec::util::Processor::Message::Meta::remove) {
                    heads.erase(std::find(begin(heads), end(heads), msg.head));
                }
            }
            usleep(20);
        }
    } catch(std::exception const& err) {
        std::cerr << err.what() << std::endl;
        return -1;
    } catch (RtAudioError& err) {
        err.printMessage();
        return -1;
    }
    return 0;
}
