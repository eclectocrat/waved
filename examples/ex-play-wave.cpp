///
/// This examples shows the playback of Wave<T> and Wave_list<T> without any
/// Head<T>'s. For an example that uses the full playback API and all it's
/// features, see ex-play-heads.cpp.
///

#include <atomic>
#include <iostream>
#include <unistd.h>
#include "util.hpp"

std::shared_ptr<ec::Wave_list<float>> list;
std::atomic<size_t> play_pos{0};

int drive_audio(void *out, void *in, unsigned int frames, double stream_time,
                RtAudioStreamStatus status, void *data) {
    std::memset(out, 0, frames * 2 * sizeof(float));
    auto output = reinterpret_cast<float*>(out);
    const auto range = ec::Range<size_t>{
        play_pos,
        play_pos + std::min<size_t>(frames, list->frame_count() - play_pos)};
    play_pos += range.size();
    ec::read_map(*list, range, 0, {output, output + range.size()});
    if (list->channel_count() > 1) {
        ec::read_map(*list, range, 1,
                 {output + range.size(), output + range.size() * 2});
    } else {
        std::memcpy(output + range.size(), output,
                    range.size() * sizeof(float));
    }
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Usage: wave-play path/to/audio/file.[wav, aif, ...]\n";
        return 0;
    }
    RtAudio dac;
    try {
        auto [settings, options] = ec::util::setup_RtAudio(dac);
        auto [stream, info] = ec::util::setup_Sndfile(argv[1]);
        list = ec::util::make_mapped_list_from_stream(stream, info);
        ec::util::start_RtAudio(dac, settings, options, drive_audio, list.get());
        while (play_pos < list->frame_count()) {
            usleep(20);
        }
    } catch(std::exception const& err) {
        std::cerr << err.what() << std::endl;
        return -1;
    } catch (RtAudioError& err) {
        err.printMessage();
        return -1;
    }
    return 0;
}
