#ifndef lua_interface_h
#define lua_interface_h

namespace ec {
namespace lua {

    /*
void Weak_self::make_weak_self(lua_State* ls) {
    lua_createtable(ls, 0, 1);          // self, {}
    lua_pushstring(ls, "v");            // self, {}, "v"
    lua_setfield(ls, -2, "__mode");     // self, {__mode="v"}
    lua_pushnil(ls);                    // self, {__mode="v"}, nil
    lua_copy(ls, -3, -1);               // self, {__mode="v"}, self
    lua_setfield(ls, -2, "self");       // self, {__mode="v", self=self}
    lua::update_ref(ls, weak_self_ref); // self
}

void Weak_self::free_weak_self(lua_State* ls) {
    if (weak_self_ref != LUA_REFNIL) {
        luaL_unref(ls, LUA_REGISTRYINDEX, weak_self_ref);
    }
}

void Weak_self::push_weak_self(lua_State *ls) {
    expects(weak_self_ref != LUA_REFNIL);
    lua_rawgeti(ls, LUA_REGISTRYINDEX, weak_self_ref);  // {}|nil
    luaL_checktype(ls, -1, LUA_TTABLE);                 // {}
    lua_getfield(ls, -1, "self");                       // {}, self|nil
    lua_remove(ls, -2);                                 // self|nil
}
*/

using Wave_ptr = std::shared_ptr<ec::Wave_list<float>>;

std::list<Wave_ptr> all_waves;

namespace { const char * Wave_type_name = "ec.MutableWave"; }

int Wave_create(lua_State *ls) {
    auto ptr_to_ptr = lua::create_object<Wave_ptr>(ls, Wave_type_name);
    *ptr_to_ptr = make_shared<Wave_list<float>>();
    all_waves.push_back(*ptr_to_ptr);
    return 1;
}

int Wave_destroy(lua_State *ls) {
    auto ptr_to_ptr = lua::destroy_object<Wave_ptr>(ls, Wave_type_name);
    if (not ptr_to_ptr) {
        lua_settop(ls, 0);
    } else if (auto i = find(begin(all_waves), end(all_waves), *ptr_to_ptr);
               i != end(all_waves)) {
        all_waves.erase(i);
    }
    return 0;
}

int Wave_clone(lua_State *ls) {
    auto clone_ptr =
        lua::clone_object(ls, Wave_type_name, lua::checked_get<Wave_ptr *>(ls, 1));
    all_waves.push_back(*clone_ptr);
    return 1;
}

int Wave_open_stream(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    self->open_stream(lua::param<string>(ls, "file_path"));
    return 0;
}

int Wave_map_stream_samples(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    if (self->empty()) {
        luaL_error(ls,
                   "[Wave] unable to map stream samples, no clips are loaded!");
    } else {
        for_each(self->clips().begin(), self->clips().end(), [&](auto &c) {
            if (not c.is_stream_open()) {
                luaL_error(
                    ls,
                    "[Wave] unable to map stream samples, no stream is open!");
            }
            // NOTE: assumes that all clips in the wave are sourced from the
            // same stream and haven't been shuffled. If _stream_range and
            // _file_stream are out of sync or heterogenous across clips, then
            // bizarro land will manifest on the tip of your cat's tail.
            c.map_stream_frames(c.range());
        });
    }
    return 0;
}

int Wave_unmap_samples(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    for_each(self->clips().begin(), self->clips().end(),
             [](auto &c) { c.unmap_frames(); });
    return 0;
}

int Wave_frame_count(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    lua_pushinteger(ls, int(self->frame_count()));
    return 1;
}

int Wave_channel_count(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    lua_pushinteger(ls, int(self->channel_count()));
    return 1;
}

int Wave_copy_mapped_samples(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    auto chan = lua::param<int>(ls, "channel");
    const auto ranges = lua::as_ranges(ls, self->frame_count(), "ranges");
    lua_createtable(ls, int(size(ranges)), 0);
    int i = 1;
    for_each(begin(ranges), end(ranges), [&](auto & range) {
        self->for_range(range, [](auto &c) { return c.range(); },
                        [&](auto &c) -> auto & { return c.samples()[chan]; },
                        [&](float f) {
                            lua_pushnumber(ls, f);
                            lua_rawseti(ls, -2, i++);
                        });
    });
    return 1;
}

int Wave_write_mapped_samples(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    auto chan = lua::param<int>(ls, "channel");
    const auto begin = lua::get_pos(ls, "to", self);
    lua_getfield(ls, -1, "buffer");
    luaL_checktype(ls, -1, LUA_TTABLE);
    lua_len(ls, -1);
    const size_t buffer_len = lua_tonumber(ls, -1);
    lua_pop(ls, 1);
    int i = 1;
    self->for_range(Sample_range(begin, begin + buffer_len),
                    [](auto &c) { return c.range(); },
                    [&](auto &c) -> auto & { return c.samples()[chan]; },
                    [&](float &f) {
                        lua_rawgeti(ls, -1, i++);
                        f = lua_tonumber(ls, -1);
                        lua_pop(ls, 1);
                    });
    return 0;
}

int Wave_remove(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    const auto ranges = lua::as_ranges(ls, self->frame_count(), "ranges");
    for_each(rbegin(ranges), rend(ranges), [&](auto & range) {
        self->erase(range);
    });
    return 0;
}

int Wave_take(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    auto from = lua::param<Wave *>(ls, "from");
    const auto ranges = lua::as_ranges(ls, self->frame_count(), "ranges");
    const auto to = lua::get_pos(ls, "to", self);
    for_each(rbegin(ranges), rend(ranges), [&](auto & range) {
        self->take(range, *from, size_t(to));
    });
    return 0;
}

int Wave_copy(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    auto from = lua::param<Wave *>(ls, "from");
    const auto ranges = lua::as_ranges(ls, self->frame_count(), "ranges");
    const auto to = lua::get_pos(ls, "to", self);
    for_each(rbegin(ranges), rend(ranges), [&](auto & range) {
        self->copy(range, *from, size_t(to));
    });
    return 0;
}

int Wave_print(lua_State *ls) {
    auto self = lua::checked_get<Wave *>(ls, 1);
    Braille_canvas canvas({100, 44});
    const auto src = self->clips().front().rms();
    const auto src_len = src[0].size();
    const auto pts_per_channel = canvas.point_size().h / self->channel_count();
    for_all_n_ranges(
        N_ranges<size_t>(src_len, canvas.point_size().w),
        [&](auto i, auto range) {
            const auto rms = min(1.f, *max_element(begin(src[0]) + begin(range),
                                                   begin(src[0]) + end(range)));
            const auto rms_h = rms * pts_per_channel / 2;
            const auto center = pts_per_channel / 2;
            canvas.set_vline(i, center, center - rms_h);
            canvas.set_vline(i, center, center + rms_h + 1);
            if (src.size() > 1) {
                const auto rms_r =
                    min(1.f, *max_element(begin(src[1]) + begin(range),
                                          begin(src[1]) + end(range)));
                const auto rms_r_h = rms_r * pts_per_channel / 2;
                const auto center_r = center + pts_per_channel;
                canvas.set_vline(i, center_r, center_r - rms_r_h);
                canvas.set_vline(i, center_r, center_r + rms_r_h + 1);
            }
        });
    canvas.draw(wcout);
    wcout.flush();
    return 0;
}

static const luaL_Reg Wave_funcs[] = {{"new", Wave_create}, {0, 0}};
static const luaL_Reg Wave_meths[] = {
    {"__gc", Wave_destroy},
    {"print", Wave_print},
    {"make_copy", Wave_clone},
    {"open_stream", Wave_open_stream},
    {"get_frame_count", Wave_frame_count},
    {"get_channel_count", Wave_channel_count},
    {"map_stream_frames", Wave_map_stream_samples},
    {"unmap_frames", Wave_unmap_samples},
    {"copy_frames", Wave_copy_mapped_samples},
    {"write", Wave_write_mapped_samples},
    {"remove", Wave_remove},
    {"take", Wave_take},
    {"copy_from", Wave_copy},
    {0, 0}};

/*
Wave(W) ::
    ; Low level mutating interface
    make_copy           :: Wave -> Wave
    open_stream         :: Wave -> String -> Void
    let frame_count     :: Int
    let channel_count   :: Int (where 0 <= channel_count <= 2)
    map_stream_frames   :: Wave -> Void
    unmap_frames        :: Wave -> Void
    copy_frames         :: Wave -> [Real]
    write               :: Wave -> [Real] -> channel:Int -> Pos -> Void
    remove              :: Wave -> Ranges -> Void
    take                :: Wave -> from:Wave -> Ranges -> Void
    copy_from           :: Wave -> from:Wave -> Ranges -> Void
 
    remove_front        :: Wave -> Pos -> Void
    remove_back         :: Wave -> Pos -> Void
    crop                :: Wave -> Range -> Void
 
    ; Functional-immutable interface
    removed     :: Wave -> remove:Ranges -> Wave
    inserted    :: Wave -> insert:Wave -> at:Pos -> Wave
    reversed    :: Wave -> Wave
    front       :: Wave -> until:Pos -> Wave
    back        :: Wave -> from:Pos -> Wave
    subranges   :: Wave -> subranges:Ranges -> Wave
    processed   :: Wave -> process:Process -> Wave
*/

void ec::register_wave_model(lua_State *lua) {
    ec::lua::register_class(lua, "Wave", WAVE_TYPE, Wave_meths, Wave_funcs);
}

#endif /* lua_interface_h */
