#ifndef ec_util_hpp
#define ec_util_hpp

#include <memory>
#include <stdexcept>
#include <sstream>
#include <rtaudio/RtAudio.h>
#include <sndfile.h>
#include <wave_list.hpp>
#include <wave_list_funcs.hpp>
#include <head_processor.hpp>
#include <queue.hpp>

namespace ec {
namespace util {

template<typename T>
using Queue = ec::Queue<T, 256>;
using Processor = Head_processor<float, Queue>;

/// \brief Sets up the default audio device with some default values.
///
/// \param dac RtAudio object to setup.
/// \return Pair of Rt::Audio::StreamParameters and RtAudio::StreamOptions.
///
auto setup_RtAudio(RtAudio &dac) {
    if (dac.getDeviceCount() < 1) {
        throw std::runtime_error("No audio devices found!");
    }
    dac.showWarnings(true);
    RtAudio::StreamParameters params;
    params.deviceId = dac.getDefaultOutputDevice();
    params.nChannels = 2;
    params.firstChannel = 0;
    RtAudio::StreamOptions options;
    options.flags = RTAUDIO_SCHEDULE_REALTIME | RTAUDIO_NONINTERLEAVED;
    return std::make_pair(params, options);
}


/// \brief  Starts the given device with the given parameters and some default
///         values for window size and sample-rate.
///
/// \param dac      Device to start.
/// \param params   Parameters to use.
/// \param options  Options to use.
/// \param callback Stream callback to use.
/// \param data     Data to pass to stream callback each window.
///
void start_RtAudio(RtAudio& dac,
                   RtAudio::StreamParameters& params, 
                   RtAudio::StreamOptions &options,
                   RtAudioCallback callback,
                   void * data) {
    unsigned int max_frames = 256;
    dac.openStream(&params, nullptr, RTAUDIO_FLOAT32, 44100, &max_frames,
                   callback, data, &options, nullptr);
    dac.startStream();
}


/// \brief Default RtAudio callback to read from a Head_processor<T>.
///
/// \see RtAudio documentation for info about the callback parameters.
///
int drive_head_processor_RtAudio(void *out, void *in, unsigned int frames,
                                 double stream_time, RtAudioStreamStatus status,
                                 void *data) {
    std::memset(out, 0, frames * 2 * sizeof(float));
    auto output = reinterpret_cast<float*>(out);
    auto outs = Span_array<float>({output, output + frames},
                                  {output + frames, output + frames * 2});
    auto& proc = *reinterpret_cast<Processor*>(data);
    proc.process(outs);
    return 0;
}


/// \brief  Opens a sound file.
///
/// \param path Path to file to open.
/// \return pair of SNDFILE and SF_INFO.
///
auto setup_Sndfile(std::string const& path) {
    // libsndfile stream setup
    SF_INFO info;
    auto stream = sf_open(path.c_str(), SFM_READ, &info);
    if (stream == NULL) {
        std::ostringstream ost;
        ost << "Error opening stream " << path  << " - " 
            << sf_strerror(NULL);
        throw std::runtime_error(ost.str());
    }
    return std::make_pair(stream, info);
}


const unsigned int max_stream_frames = 4096;

/// \brief Hook between libwaved and libsndfile.
///
/// \param stream       File to read from.
/// \param stream_info  Stream info of given file.
/// \return function (lambda) that reads from the stream, and can be passed to
///         Wave<T>::set_stream.
///
auto make_stream(SNDFILE* stream, SF_INFO stream_info) {
    static float deinterleave[max_stream_frames * 2];

    return [=](Range<size_t> range, Span_array<float> out) 
           -> Span_array<float> {
        assert(stream);
        assert(range.size() <= out.span_size());
        assert(range.size() <= max_stream_frames);
        if (stream_info.channels == 1) {
            const auto read =
                size_t(sf_readf_float(stream, out[0].data(), range.size()));
            return out.subspans({0, read});
        } else {
            assert(stream_info.channels == 2);
            const auto read =
                size_t(sf_readf_float(stream, deinterleave, range.size()));
            size_t k = 0;
            for (size_t i = 0; i < range.size(); ++i) {
                for (size_t j = 0; j < stream_info.channels; ++j) {
                    out[j][i] = deinterleave[k++];
                }
            }
            return out.subspans({0, read});
        }
    };
}


/// \brief  Makes a list with a single wave that has given stream.
///         
/// \param stream   Stream to map into a list.
/// \param info     Info for given stream.
/// \return New list with one wave with the given stream and no map.
///
std::shared_ptr<Wave_list<float>> 
make_unmapped_list_from_stream(SNDFILE* stream, SF_INFO info) {
    auto list = std::make_shared<Wave_list<float>>();
    list->insert(end(*list), 
                 Wave<float>(make_stream(stream, info), info.channels, 
                             {0, size_t(info.frames)}));
    return list;
}


/// \brief  Makes a list with a single wave with the mapped contents of the
///         given stream.
///
/// \param stream   Stream to map into a list.
/// \param info     Info for given stream.
/// \return New list with one wave with the given stream and it's entire frame
///         count mapped in memory.
std::shared_ptr<Wave_list<float>> 
make_mapped_list_from_stream(SNDFILE* stream, SF_INFO info) {
    auto list = make_unmapped_list_from_stream(stream, info);
    list->front().set_map(make_uninitialized_buffers<float>(info.channels, 
                                                            info.frames));
    list->front().map_stream_frames(max_stream_frames);
    return list;
}

} // namespace util
} // namespace ec
#endif // ex_util.hpp
