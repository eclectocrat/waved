#ifndef _ec_views_hpp
#define _ec_views_hpp

#include <iostream>
#include <vector>
#include <wave_list_funcs.hpp>

namespace ec {
namespace views {

/// Pixel display.
struct Pix_view {
    Pix_view(size_t char_width, size_t char_height) {
        expects(char_width > 0);
        expects(char_height > 0);
        _canvas.resize(char_height);
        for (auto &v: _canvas) { v.resize(char_width); }
    }
    size_t char_width() const { return _canvas.empty() ? 0 : _canvas[0].size(); }
    size_t char_height() const { return _canvas.size(); }
    size_t pix_width() const { return char_width() * 2; }
    size_t pix_height() const { return char_height() * 4; }
    void set_pix(size_t x, size_t y) {
        if (x > _canvas[0].size() * 2 or x < 1) { x = 0; }
        if (y > _canvas.size() * 4 or y < 1) { y = 0; }
        _canvas[y / 4][x / 2] |= _pixmap[y % 4][x % 2];
    }
    void unset_pix(size_t x, size_t y) {
        if (x > _canvas[0].size() * 2 or x < 1) { x = 0; }
        if (y > _canvas.size() * 4 or y < 1) { y = 0; }
        _canvas[y / 4][x / 2] &= ~_pixmap[y % 4][x % 2];
    }

private:
    friend std::wostream& operator << (std::wostream&, Pix_view&);
    constexpr static size_t _pixmap[4][2] = {
        {0x01, 0x08}, {0x02, 0x10}, {0x04, 0x20}, {0x40, 0x80}};
    constexpr static wchar_t _braille = 0x2800;
    std::vector<std::vector<wchar_t>> _canvas;
};

std::wostream& operator << (std::wostream& wost, Pix_view& pix_view) {
    for (auto &v : pix_view._canvas) {
        for (auto &c : v) {
            if (c == 0) { 
                wost << " "; 
            } else { 
                wost << std::wstring { pix_view._braille + c }; 
            }
        }
        wost << std::endl;
    }
    return wost;
}

/// \brief Calculates the root mean square of the given range.
///
/// \param begin    Beginning of range.
/// \param end      End of range.
/// \return The root mean square of the range.
///
template <typename Begin, typename End>
float root_mean_square(Begin begin, End end) {
    expects(begin != end);
    const auto total = std::accumulate(
        begin, end, 0.0,
        [](auto base, auto elem) { return base + elem * elem; });
    return std::sqrt(total / std::distance(begin, end));
}

/// RMS view.
struct RMS_view: public Pix_view {
    RMS_view(size_t char_width, size_t char_height)
        : Pix_view(char_width, char_height) {}

    void build_rms_sync(ec::Wave_list<float>& wave) {
        _alloc_rms(wave.channel_count());
        const auto n_ranges = ec::N_ranges<size_t>(wave.frame_count(), pix_width());
        auto read_bufs = ec::util::make_uninitialized_buffers<float>(
                                                1, n_ranges.range_max_size());
        for (size_t chan=0; chan<wave.channel_count(); ++chan) {
            ec::for_all_n_ranges(n_ranges, [&](size_t i, Range<size_t> rng) {
                ec::read_map(wave, rng, i, read_bufs[0]);
                _rms[chan][i] = root_mean_square(
                    begin(read_bufs[0]), begin(read_bufs[0]) + rng.size());
            });
        }
    }
    void draw_rms() {
        float scale = 1.f;
        //if (_scale_fill) {
        //    const auto max = std::max(begin(_rms), end(_rms));
        //    scale *= *max;
        //}
        draw_rms(scale);
    }
    void draw_rms(float scale) {
        for (size_t channel=0; channel < _rms.size(); ++channel) {
            expects(_rms[channel].size() == pix_width());
            for (size_t x=0; x<pix_width(); ++x) {
                const auto extent =
                    _pix_y_pos_for_channel(channel) +
                    _rms[channel][x] * scale * _pix_height_per_channel();
                for (size_t y = _pix_y_pos_for_channel(channel);
                     y < extent; ++y) {
                    set_pix(x, y);
                }
            }
        }
    }

private:
    size_t _pix_y_pos_for_channel(size_t chan) const {
        if (_rms.size() == 1) {
            return 0;
        } else {
            return (chan * pix_height()) / _rms.size();
        }
    }
    size_t _pix_height_per_channel() const {
        return pix_height() / _rms.size();
    }
    void _alloc_rms(size_t channels) {
        _rms.clear();
        for (size_t chan = 0; chan < channels; ++chan) {
            _rms.push_back(ec::Buffer<float>{new float[pix_width()], pix_width(),
                                             [](auto p, auto) { delete[] p; }});
        }
    }
    std::vector<ec::Buffer<float>> _rms;
    bool _scale_fill = true;
};

} // namespace views
} // namespace ec
#endif // _ec_views_hpp
