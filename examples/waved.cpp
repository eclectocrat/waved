///
/// This examples shows the playback of Wave<T> and Wave_list<T> using multiple
/// Head<T>'s in a Head_processor<T>.
///

#include <atomic>
#include <iostream>
#include <vector>
#include <unistd.h>
#include <wave_list_funcs.hpp>
#include "util.hpp"
#include "views.hpp"

///////////////////////
// Application State //
///////////////////////

std::shared_ptr<ec::Wave_list<float>> list;
std::shared_ptr<ec::Head<float>> head;
ec::util::Processor proc;

///////////////////////
// Application Entry //
///////////////////////

int main(int argc, char *argv[]) {
    std::locale::global(std::locale(""));
    if (argc < 2) {
        std::cout << "Usage: waved path/to/audio/file.[wav, aif, ...]\n";
        return 0;
    }
    RtAudio dac;
    try {
        auto [settings, options] = ec::util::setup_RtAudio(dac);
        
        // load wave
        auto [stream, info] = ec::util::setup_Sndfile(argv[1]);
        list = ec::util::make_mapped_list_from_stream(stream, info);

        // draw wave
        ec::views::RMS_view view(80, 24);
        view.build_rms_sync(*list);
        view.draw_rms();
        std::wcout << view;

        // play wave
        head = std::make_shared<ec::Head<float>>();
        head->attach(list, {0, list->frame_count()}, 0);
        proc.prepare_before_processing(16, 256, 4096);
        using Message = ec::util::Processor::Message;
        proc.send_message({&proc, head, Message::Meta::add});
        ec::util::start_RtAudio(dac, settings, options,
                                ec::util::drive_head_processor_RtAudio, &proc);
        
        // wait until done
        bool done = false;
        while (not done) {
            ec::util::Processor::Message msg;
            while (proc.try_recieve_message(msg)) {
                done = msg.meta == Message::Meta::remove;
            }
            usleep(20);
        }
    } catch(std::exception const& err) {
        std::cerr << err.what() << std::endl;
        return -1;
    } catch (RtAudioError& err) {
        err.printMessage();
        return -1;
    }
    return 0;
}
