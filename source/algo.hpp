#ifndef ec_algo_hpp
#define ec_algo_hpp

#include "expects.hpp"
#include "function.hpp"
#include "range.hpp"
#include "span.hpp"
#include "span_array.hpp"
#include "n_ranges_for.hpp"

namespace ec {

/// \brief  Copies the first and every `stride` elements after it in a random
///         access range.
///
/// \param b        Beginning of the range to copy. This element is always
///                 copied if the range given was not empty.
/// \param e        End of range to copy from.
/// \param stride   The stride of the copy. This is the number of elements
///                 jumped between each read.
///
template<typename I1, typename I2, typename I3, typename Uint_t>
constexpr void copy_stride(I1 b, I2 e, Uint_t stride, I3 d) {
    for (; b<e; b+=stride) {
        *d++ = *b;
    }
}


/// \brief Fades a buffer if we are near the extents of a range.
///
/// The Read_head class uses this function to smooth out loops.
///
/// \param forward      True if the logical direction is from begin to end,
///                     false if it is from end to begin.
/// \param range        Range that gives the extents for begin and end.
/// \param fade_dur     Number of frames the fade is to be applied over.
/// \param buffers      Contains the memory to possible apply a fade over.
///
template <typename T, size_t N>
void fade_if_near_ends(bool forward, Range<size_t> extents, Range<size_t> range,
                       size_t fade_dur, Span_array<T, N> buffers) {
    // fade out end
    const auto pos = forward
        ? std::make_pair(to(range), to(extents) - to(range))
        : std::make_pair(from(range), from(range) - from(extents));
    if (pos.second < fade_dur) {
        const auto f = Discrete_function(
            1.0, 0.0, split_range(extents, extents.size() - fade_dur).second);
        buffers.per_span([&](auto, auto &b) {
            f(to(extents) - pos.second,
              begin(b) + std::max(int(pos.second) - int(pos.first), 0),
              end(b));
        });
    }
    // fade in begin
    const auto frames_from_begin = forward
        ? to(range) - from(extents)
        : to(extents) - from(range);
    if (frames_from_begin < fade_dur) {
        const auto f =
            Discrete_function(0.0, 1.0, split_range(extents, fade_dur).first);
        buffers.per_span([&](auto, auto &b) {
            f(from(extents) + frames_from_begin, begin(b),
              begin(b) + std::min(int(fade_dur) - int(frames_from_begin),
                                  int(buffers.frame_count())));
        });
    }
}


/// \brief Transforms an input of X frames into an output of Y frames.
///
/// \param speed    Speed of the output, i.e. how many inputs per outputs.
/// \param amp      Amp mod to apply to all output samples.
/// \param inp      Input samples.
/// \param outp     Time transformed output samples.
///                 Must have a size >= input sample count * speed.
/// \return Length of the output signal.
///
template <typename T, size_t N>
size_t time_transform(float speed, float amp, Span_array<T, N> inp,
                      Span_array<T, N> outp) {
    // TODO: select interpolation type.
    N_ranges<size_t> gen(
        inp[0].size(),
        std::max(size_t(1), std::min(size_t(inp[0].size() / speed),
                                     size_t(outp[0].size()))));
    if (inp.size() > 1) { // stereo path
        for_all_n_ranges(gen, [&](auto i, auto r) {
            outp.per_span([&](auto c, auto &) {
                outp[c][i] = inp[c][from(r) - (1 - r.size())] * amp;
            });
        });
    } else { // mono
        for_all_n_ranges(gen, [&](auto i, auto r) {
            if (r.size() < 2) {
                outp[0][i] = inp[0][from(r) - (1 - r.size())] * amp;
            } else {
                outp[0][i] = std::accumulate(begin(inp[0]) + from(r),
                                             begin(inp[0]) + to(r), 0.0) /
                             float(r.size()) * amp;
            }
        });
    }
    return gen.ranges_size();
}

} // namespace
#endif // ec_algo_h
