#ifndef ec_buffer_hpp
#define ec_buffer_hpp

#include "view.hpp"
#include <memory>
#include <algorithm>

namespace ec {

template <typename T> struct Buffer {
    constexpr Buffer() = default;
    constexpr Buffer(Buffer const &) = default;
    Buffer &operator=(Buffer const &) = default;

    template<typename F>
    Buffer(T * p, const size_t n, F release):
        _data(p, [=](auto del) {
            release(del, n);
        }), _range(0, n) {
        expects(p);
        expects(n > 0);
    }
    
    View<T> view() { return View<T>(_data, _range); }
    View<const T> view() const { return View<T>(_data, _range); }
    constexpr T *data() { return _data ? _data.get() + from(_range) : nullptr; }
    constexpr T const *data() const { return _data ? _data.get() + from(_range) : nullptr; }
    constexpr size_t size() const { return _range.size(); }
    constexpr bool empty() const { return _range.size() == 0; }

    Buffer slice(Range<size_t> r) const TESTEXCEPT {
        expects(_range.size() >= to(r));
        auto buff = *this;
        buff._range = offset(r, from(_range));
        return buff;
    }
    Buffer split_front(const size_t off) TESTEXCEPT {
        expects(off < size());
        auto ret = slice({from(_range), from(_range) + off});
        _range = {from(_range) + off, to(_range)};
        return ret;
    }
    Buffer split_back(const size_t off) TESTEXCEPT {
        expects(off < size());
        auto ret = slice({from(_range) + off, to(_range)});
        _range = {from(_range), from(_range) + off};
        return ret;
    }

private:
    template<typename F>
    void init(T * p, const size_t count, F release) {
        expects(count > 0);
        _data = std::shared_ptr<T>(p,
                             [=](auto p) {
                                release(p, count);
                             });
        _range = make_range(size_t(0), count);
    }

    std::shared_ptr<T> _data;
    Range<size_t> _range;
};

namespace util {
template<typename T>
std::vector<Buffer<T>> make_uninitialized_buffers(size_t num, size_t length) {
    std::vector<Buffer<T>> buffs;
    buffs.reserve(num);
    while (num--) {
        buffs.push_back(
            {new T[length], length, [](auto p, auto) { delete [] p; }});
    }
    return std::move(buffs);
}
} // namespace util
} // namespace ec
#endif // ec_buffer_hpp
