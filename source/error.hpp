#ifndef _ec_error_hpp
#define _ec_error_hpp

namespace ec {

#ifndef EC_DISABLE_EXCEPTIONS
template<typename T> [[ noreturn ]] void fail(T const& error) {
    throw error;
}

struct Failure {};
[[ noreturn ]] inline void fail() { throw Failure{}; }
#else
#pragma "Implement your own fail() function in namespace ec before including this module, or re-enable exceptions."
#endif

} // namespace
#endif // _ec_error_hpp
