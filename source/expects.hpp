#ifndef ec_expects_hpp
#define ec_expects_hpp

#include <type_traits>
#include <cstddef>
#include <cassert>

#ifndef TESTEXCEPT
#define TESTEXCEPT noexcept
#endif

namespace ec {

struct Unexpected {};

constexpr void expects(const bool test) {
    if (not test) {
        throw Unexpected{};
    }
}

// TODO: replace macro
#define static_expects(b) \
    static_assert(b, "static_expects() failed in DEBUG mode.")

template<typename T, typename = std::enable_if<std::is_integral<T>::value>>
struct Expects_nonzero {
    template <typename U>
    constexpr Expects_nonzero(U const& v): _value(v) { expects(_value != 0); }
    constexpr Expects_nonzero(T const& v): _value(v) { expects(_value != 0); }
    constexpr Expects_nonzero(Expects_nonzero const&) = default;
    Expects_nonzero& operator=(Expects_nonzero const&) = default;
    constexpr operator T() const { return _value; }
    constexpr T value() const { return _value; }
    constexpr T& weak() { return _value; }
private:
    T _value = T();
};

template<typename T>
using Expects_nonnull_ptr = Expects_nonzero<T*>;

template<typename T, size_t Begin, size_t End,
         typename = std::enable_if<std::is_integral<T>::value>>
struct Expects_within {
    template <typename U>
    constexpr Expects_within(U const& v): _value(v) {
        expects(_value >= Begin);
        expects(_value < End);
    }
    constexpr Expects_within(T const& v): _value(v) {
        expects(_value >= Begin);
        expects(_value < End);
    }
    constexpr Expects_within(Expects_within const&) = default;
    Expects_within& operator=(Expects_within const&) = default;
    constexpr operator T() const { return _value; }
    constexpr T value() const { return _value; }
    constexpr T& weak() { return _value; }
private:
    T _value = T();
};

} // namespace
#endif // ec_expects_hpp
