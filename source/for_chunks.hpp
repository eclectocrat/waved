#ifndef _for_chunks_hpp
#define _for_chunks_hpp

#include <algorithm>
#include <numeric>
#include <iterator>
#include "expects.hpp"
#include "range.hpp"

namespace ec {
/// \brief Processes a list of elements by largest available contiguous chunk.
///
/// Generic algorithm to process a range of elements in a container of
/// containers of elements, where each sub-container extends the previous
/// sub-container, resulting in an aggregate container with an aggregate sum
/// size(). i.e: std::list<vector<int>>, or std::vector<MyType>.
///
/// This is a higher-order function that takes the interface to the sublist
/// elements along with the list itself. You must pass two interfaces:
/// range_f()->Range<T> which returns the range of the sub-container in the
/// overall aggregate container, and buffer_f()-> which returns a range of
/// elements (anything returning begin() and end() iterators).
///
/// \param list     List of lists to process by chunk.
/// \param range    Aggregate range to process, i.e. the range in terms of the
///                 lengths of each sublist added together.
/// \param range_f  Function from typeof(List[]) -> Range<T>.
/// \param buffer_f Function from typeof(List[]) -> <C++ range (begin/end)>.
/// \param func     Function to apply to each range: (r: Range, b: <iter>, e: <iter>) -> void.
///                 r is zero based range (ie. 0 in this range == from(range)). TODO: ??? REALLY ???
///
template <typename List, typename Range, typename F, typename Range_f,
          typename Buffer_f>
void for_chunks(List &list, Range range, Range_f range_f, Buffer_f buffer_f,
                F func) {
    using namespace std;
    expects(not list.empty());
    expects(to(range_f(list.back())) >= to(range));
    auto b = lower_bound(begin(list), end(list),
                         typename List::value_type(from(range)),
                         [&](auto const &a, auto const &b) {
                             return to(range_f(a)) < to(range_f(b));
                         });
    auto e = upper_bound(begin(list), end(list),
                         typename List::value_type(to(range)),
                         [&](auto const &a, auto const &b) {
                             return from(range_f(a)) < from(range_f(b));
                         });
    if (b == e) {
        expects(e != end(list));
        const auto r = range_f(*b);
        auto&& buf = buffer_f(*b);
        // (A) error here means the function you passed in cannot accept the
        // params given: Range -> Iter -> Iter -> Void
        func(range, begin(buf) + (from(range) - from(r)),
                    begin(buf) + (to(range) - from(r)));
    } else {
        while (b != e) {
            const auto r = range_f(*b);
            const auto rng = intersection(range, r);
            auto&& buf = buffer_f(*b);
            // (B) if you get an error here: see A above.
            func(rng, begin(buf) + (from(rng) - from(r)),
                      begin(buf) + (to(rng) - from(r)));
            ++b;
        }
    }
}

} // namespace ec
#endif // _for_chunks_hpp 
