#ifndef ec_head_hpp
#define ec_head_hpp

#include "wave_list.hpp"
#include "algo.hpp"
#include "function.hpp"
#include "n_ranges_for.hpp"
#include <atomic>
#include <tuple>

namespace ec {

template <typename T> struct Head {
    using Sample = T;
    using Wave = Wave<T>;
    using Wave_list = Wave_list<T>;
    using Range = typename Wave::Range;
    using Buffer = typename Wave::Buffer;
    using Span = typename Wave::Span;
    using Span_array = typename Wave::Span_array;
    enum class Direction { forward, backward };
    using Snapshot = std::tuple<double, Direction, Range, Range>;

    // TODO: I hate these
    static const size_t max_frames_per_slice = 512;
    static const size_t max_buffer_size = 4096;

    void attach(std::shared_ptr<Wave_list> const &list, Range range, size_t pos) {
        expects(list != nullptr);
        expects(Range{0, list->frame_count()}.contains(range));
        _range_from = from(range);
        _range_to = to(range);
        _pos = pos;
        expects(_pos >= _range_from);
        expects(_pos <= _range_to);
        expects(list->channel_count() <= 2);
        _list = list;
    }
    void detach() { _list = nullptr; }

    std::shared_ptr<Wave_list> const &   list()          { return _list; }
    std::shared_ptr<const Wave_list>     list() const    { return _list; }
    
    size_t      pos         () const        { return _pos.load(); }
    void        set_pos     (size_t p)      { _pos = p; }
    Direction   dir         () const        { return _dir.load(); }
    void        set_dir     (Direction d)   { _dir = d; }
    float       amp         () const        { return _amp_mod.load(); }
    void        set_amp     (float f)       { _amp_mod = f; }
    int         loop        () const        { return _loop.load(); }
    void        set_loop    (int n)         { _loop = n; }
    size_t      range_from  () const        { return _range_from.load(); }
    size_t      range_to    () const        { return _range_to.load(); }
    Range       range       () const        { return {_range_from, _range_to}; }
    double      speed       () const        { return _speed.load(); }
    bool        remove_at_extent() const    { return _remove_at_extent.load(); }
    void        set_remove_at_extent(bool b){ _remove_at_extent = b; }
    int         mark        () const        { return _mark.load(); }
    void        set_mark    (int m)         { _mark = m; }
    
    void set_speed(double d) {
        // max_buffer_size won't support speeds lower than this
        // TODO: fix this
        d = std::max(d,
                1.0 / (double(max_buffer_size) / max_frames_per_slice) +
                    __DBL_EPSILON__ + __DBL_EPSILON__);
        _speed = d;
    }
    
    Snapshot move(size_t amt) {
        expects(amt > 0 and amt <= max_frames_per_slice);
        const auto speed = _speed.load();
        const auto dir = _dir.load();
        const auto f = _range_from.load();
        const auto t = _range_to.load();
        const auto range = Range(std::min(f, t), std::max(f, t));
        const int n = ceil(amt * speed);
        size_t pos = 0;
        int jump = 0;
        do {
            pos = _pos.load();
            jump = dir == Direction::forward
                       ?   std::min(n, int(to(range) - pos))
                       : -(std::min(n, int(pos - from(range))));
        } while (not _pos.compare_exchange_strong(pos, pos + jump));
        const auto end_pos = size_t(int(pos) + jump);
        const auto r = Range(std::min(pos, end_pos), std::max(pos, end_pos));
        expects(range.contains(r));
        return std::make_tuple(speed, dir, range, r);
    }

    std::pair<Range, size_t> read(Span_array out, Span_array buffers,
                                  Snapshot const &snap) {
        using namespace std;
        expects(_list->channel_count() == out.size());
        expects(_list->channel_count() == buffers.size());
    
        auto [speed, dir, range, r_] = snap;
        // TODO: Remove once fixed: work around for C++ BUG that doesnt
        // allow references to local bindings to be used in lambdas.
        const auto r = r_;
        size_t out_size = r.size();
        if (not r.empty()) {
            const auto amp = _amp_mod.load();
            auto spans = buffers.subspans({0, size_t(r.size())});
            spans.per_span([&](auto c, auto &s) { read_map(*_list, r, c, s); });
            if (dir == Direction::backward) {
                spans.per_span([](size_t, auto &b) { reverse(begin(b), end(b)); });
            }
            if (abs(speed - 1.0) < __DBL_EPSILON__) { // approx-equal
                spans.per_span([&](auto c, auto &b) {
                    transform(begin(b), end(b), begin(out[c]),
                              [=](float f) { return f * amp; });
                });
            } else {
                out_size = time_transform(speed, amp, spans, out);
                expects(out_size <= out.span_size());
            }
            fade_if_near_ends(dir == Direction::forward, range, r, _fade_dur, out);
        }
        return {r, out_size};
    }

    std::pair<Range, size_t> move_and_read(Span_array out, Span_array buffers) {
        return read(out, buffers, move(out.span_size()));
    }
    
    Range write(Span_array in, Snapshot const& snap) {
        using namespace std;
        expects(_list->channel_count() == in.size());

        auto [speed, dir, range, r] = snap;
        // TODO: Remove once fixed: work around for C++ BUG that doesnt
        // allow references to local bindings to be captured.
        if (not r.empty()) {
            const auto amp = _amp_mod.load();
            for(int c = 0; c < in.size(); ++c) {
                int i = 0;
                _list->for_samples(r, c, [&](auto& sample) {
                    sample = in[c][i++] * amp;
                });
            }
        }
        return r;
    }

    Range move_and_write(Span_array in) {
        return write(in, move(in.span_size()));
    }
    
private:
    std::shared_ptr<Wave_list> _list;
    std::atomic<Direction>  _dir                {Direction::forward};
    std::atomic<size_t>     _range_from         {0};
    std::atomic<size_t>     _range_to           {0};
    std::atomic<int>        _loop               {0};
    std::atomic<size_t>     _pos                {0};
    std::atomic<double>     _speed              {1.0};
    std::atomic<float>      _amp_mod            {1.0};
    std::atomic<size_t>     _fade_dur           {512};
    std::atomic<bool>       _remove_at_extent   {true};
    std::atomic<int>        _mark               {0};
};

} // namespace
#endif // ec_head_hpp
