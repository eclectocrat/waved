#ifndef ec_head_processor_hpp
#define ec_head_processor_hpp

#include <memory>
#include <vector>
#include "error.hpp"
#include "head.hpp"
#include "function.hpp"

namespace ec {

template<typename T, template<typename U> class Q>
struct Head_processor {
    using Sample = T;
    using Head = Head<T>;
    using Wave = typename Head::Wave;
    using Wave_list = typename Head::Wave_list;
    using Buffer = Buffer<Sample>;
    using Span = Span<T>;
    using Span_array = Span_array<T, 2>;
    using Range = typename Wave::Range;

    struct Message {
        enum Meta {
            add, remove, played,
            stream_will_begin_slice, stream_did_end_slice
        };
        Head_processor * processor = nullptr;
        std::shared_ptr<Head> head;
        Meta meta = add;
        Range range;
    };

    void process(Span_array out) {
        using namespace std;
        const size_t frame_count = out.span_size();
        expects(frame_count > 0);
        expects(_read_bufs.span_size() >= frame_count);
        
        try_send_from({this, nullptr, Message::stream_will_begin_slice});
        process_messages(frame_count);

        for_each(begin(_heads), end(_heads), [&](auto &h) {
            const auto [read_range, read_amt] = process_head(h, nullptr, out);
            if (read_amt < frame_count and h->remove_at_extent()) { // WRONG!
                h->set_mark(1);
            }
            if (read_amt > 0) {
                try_send_from({this, h, Message::played, read_range});
            }
        });
        auto i = remove_if(begin(_heads), end(_heads),
                           [](auto const& h) { return h->mark(); });
        for_each(i, end(_heads), [&](auto & h) {
            h->set_mark(0);
            try_send_from({this, h, Message::remove});
        });
        _heads.erase(i, _heads.end());

        auto fade = [&](auto& f, auto& s, auto meta) {
            expects(s != nullptr);
            const auto [r, _] = process_head(f.head, &f, out);
            try_send_from({this, f.head, Message::played, r});
            try_send_from({this, f.head, meta});
        };
        for_each(begin(_fade_ins), end(_fade_ins), [&](auto &f) {
            fade(f, f.head->list(), Message::add);
            _heads.push_back(f.head);
        });
        _fade_ins.clear();
        for_each(begin(_fade_outs), end(_fade_outs), [&](auto &f) {
            fade(f, f.head->list(), Message::remove);
        });
        _fade_outs.clear();

        try_send_from({this, nullptr, Message::stream_did_end_slice});
    }

    void send_message(Message const &msg) {
        if (not _to_processor.try_push(msg)) {
            fail("to audio queue overflow");
        }
    }
    
    bool try_recieve_message(Message &msg) {
        return _from_processor.try_pop(msg);
    }
    
    void prepare_before_processing(size_t max_concurrent_heads,
                                   size_t slice_size,
                                   size_t timestretch_size) {
        _heads.reserve(max_concurrent_heads);
        _fade_outs.reserve(max_concurrent_heads);
        _fade_ins.reserve(max_concurrent_heads);
        auto del = [](auto p, auto){ delete [] p; };
        _small_bufs = {Buffer(new Sample[slice_size], slice_size, del),
                       Buffer(new Sample[slice_size], slice_size, del)};
        _large_bufs = {
            Buffer(new Sample[timestretch_size], timestretch_size, del),
            Buffer(new Sample[timestretch_size], timestretch_size, del)};
        _read_bufs = {_small_bufs[0], _small_bufs[1]};
        _timestretch_bufs = {_large_bufs[0], _large_bufs[1]};
    }

    void sort_best_memory_order() {
        sort(begin(_heads), end(_heads), [](auto a, auto b) {
            return a->attached() != b->attached()
                       ? a->attached() < b->attached()
                       : a->pos() < b->pos();
        });
    }

private:
    struct Fade {
        std::shared_ptr<Head> head;
        const Discrete_function func;
    };
    
    void try_send_from(Message const &msg) {
        if (not _from_processor.try_push(msg)) {
            fail("from audio queue overflow");
        }
    }

    void queue_fade(std::vector<Fade>& q, Fade const& f) {
        if (q.size() >= q.capacity()) {
            fail("fade queue overflow");
        } else {
            q.push_back(f);
        }
    }

    void process_messages(size_t slice_size) {
        Message msg;
        while (_to_processor.try_pop(msg)) {
            expects(msg.head != nullptr);
            expects(msg.head->list() != nullptr);
            switch (msg.meta) {
            case Message::add:
                if (find(begin(_heads), end(_heads), msg.head) == end(_heads)) {
                    queue_fade(_fade_ins, {msg.head,
                               Discrete_function(0.f, 1.f, {0, slice_size})});
                } break;
            case Message::remove:
                if (auto i = find(begin(_heads), end(_heads), msg.head);
                    i != end(_heads)) {
                    queue_fade(_fade_outs, {*i,
                               Discrete_function(1.f, 0.f, {0, slice_size})});
                    _heads.erase(i);
                } break;
            }
            try_send_from(msg); // echo
        }
    }

    std::pair<Range, size_t> process_head(std::shared_ptr<Head> head,
                                          Fade *fade, Span_array out) {
        expects(head != nullptr);
        expects(out.span_size() > 0);
        // read to intermediate buffer, via timestretch buffer
        const size_t chans = head->list()->channel_count();
        auto bufs = _read_bufs.subspans({0, out.span_size()}, chans);
        const auto [read_range_, read_amt_] =
            head->move_and_read(bufs, _timestretch_bufs.channels(chans));
        const auto read_range = read_range_; // TODO
        const auto read_amt = read_amt_;
        // loop and read again if needed
        size_t extra_read = 0;
        if (read_range.size() < bufs.span_size() and head->loop() > 0) {
            head->set_pos(head->dir() == Head::Direction::backward
                              ? head->range_to()
                              : head->range_from());
            extra_read = head->move_and_read(bufs.subspans({read_range.size(),
                                                            out.span_size()},
                                                           chans),
                                             _timestretch_bufs.channels(chans))
                             .second;
        }
        if (fade) {
            bufs.per_span([&](auto c, auto &s) {
                fade->func(0, begin(s), begin(s) + read_amt + extra_read);
            });
        }
        // mix to out, spread to stereo if needed
        out.per_span([&](auto c, auto &s) {
            const auto n = c < chans ? c : 0;
            std::transform(
                begin(bufs[n]), begin(bufs[n]) + read_amt + extra_read,
                begin(s), begin(s), [](auto a, auto b) { return a + b; });
        });
        return {read_range, read_amt};
    }

    Q<Message> _to_processor;
    Q<Message> _from_processor;
    std::vector<Fade> _fade_outs;
    std::vector<Fade> _fade_ins;
    std::vector<std::shared_ptr<Head>> _heads;
    std::array<Buffer, 2> _small_bufs;
    std::array<Buffer, 2> _large_bufs;
    Span_array _read_bufs;
    Span_array _timestretch_bufs;
};

} // namespace ec
#endif // ec_head_processor_hpp
