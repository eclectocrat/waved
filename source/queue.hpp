#ifndef ec_queue_hpp
#define ec_queue_hpp

#include <array>
#include <atomic>
#include "expects.hpp"

namespace ec {

template <typename T, size_t Size> struct Queue {
    Queue() {
        expects(std::atomic<size_t>().is_lock_free());
    }

    Queue(Queue const&) = delete;
    Queue& operator=(Queue const&) = delete;

    bool empty() const noexcept {
        return _rpos.load() == _wpos.load();
    }

    bool try_push(T const& t) {
        const size_t prev_pos = _wpos.load();
        const size_t new_pos = prev_pos >= Size - 1 ? 0 : prev_pos+1;
        const size_t read_pos = _rpos.load();
        if (new_pos == read_pos) {
            return false;
        } else {
            _buffer[prev_pos] = t;
            _wpos.store(new_pos);
            return true;
        }
    }

    bool try_pop(T &t) {
        if (empty()) {
            return false;
        } else {
        const auto pos = _rpos.load();
        t = std::move(_buffer[pos]);
        _rpos.store(pos >= Size - 1 ? 0 : pos+1);
        return true;
        }
    }

private:
    std::array<T, Size + 1> _buffer;
    std::atomic<size_t> _wpos{0};
    std::atomic<size_t> _rpos{0};
};

} // namespace
#endif // ec_queue_hpp
