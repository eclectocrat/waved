////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2015 Microsoft Corporation. All rights reserved.
//
// This code is licensed under the MIT License (MIT).
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef GSL_SPAN_H
#define GSL_SPAN_H

#include "expects.hpp"

#include <algorithm> // for lexicographical_compare
#include <array>     // for array
#include <cstddef>   // for ptrdiff_t, size_t, nullptr_t
#include <iterator>  // for reverse_iterator, distance, random_access_...
#include <limits>
#include <stdexcept>
#include <type_traits> // for enable_if_t, declval, is_convertible, inte...
#include <utility>

namespace ec {

constexpr const std::ptrdiff_t dynamic_extent = -1;

template <class ElementType, std::ptrdiff_t Extent = dynamic_extent>
class Span;

// implementation details
namespace details {
    template <class T>
    struct is_span_oracle : std::false_type {};

    template <class ElementType, std::ptrdiff_t Extent>
    struct is_span_oracle<Span<ElementType, Extent>> : std::true_type {};

    template <class T>
    struct is_span : public is_span_oracle<std::remove_cv_t<T>> {};

    template <class T>
    struct is_std_array_oracle : std::false_type {};

    template <class ElementType, std::size_t Extent>
    struct is_std_array_oracle<std::array<ElementType, Extent>> : std::true_type {};

    template <class T>
    struct is_std_array : public is_std_array_oracle<std::remove_cv_t<T>> {};

    template <std::ptrdiff_t From, std::ptrdiff_t To>
    struct is_allowed_extent_conversion
        : public std::integral_constant<bool,
            From == To
            or From == ec::dynamic_extent
            or To == ec::dynamic_extent> {};

    template <class From, class To>
    struct is_allowed_element_type_conversion
        : public std::integral_constant<bool, std::is_convertible<From (*)[], To (*)[]>::value> {};

    template <class Span, bool IsConst>
    class span_iterator {
        using element_type_ = typename Span::element_type;

    public:
        using iterator_category = std::random_access_iterator_tag;
        using value_type = std::remove_cv_t<element_type_>;
        using difference_type = typename Span::index_type;
        using reference = std::conditional_t<IsConst, const element_type_, element_type_>&;
        using pointer = std::add_pointer_t<reference>;

        span_iterator() = default;

        constexpr span_iterator(const Span* span, typename Span::index_type idx) noexcept
            : span_(span), index_(idx) {}

        friend span_iterator<Span, true>;
        template<bool B, std::enable_if_t<!B and IsConst>* = nullptr>
        constexpr span_iterator(const span_iterator<Span, B>& other) noexcept
            : span_iterator(other.span_, other.index_) {}

        constexpr reference operator*() const {
            expects(index_ != span_->size());
            return *(span_->data() + index_);
        }

        constexpr pointer operator->() const {
            expects(index_ != span_->size());
            return span_->data() + index_;
        }
        
        constexpr pointer raw() const {
            expects(index_ != span_->size());
            return span_->data() + index_;
        }

        constexpr span_iterator& operator++() {
            expects(0 <= index_ and index_ != span_->size());
            ++index_;
            return *this;
        }

        constexpr span_iterator operator++(int) {
            auto ret = *this;
            ++(*this);
            return ret;
        }

        constexpr span_iterator& operator--() {
            expects(index_ != 0 and index_ <= span_->size());
            --index_;
            return *this;
        }

        constexpr span_iterator operator--(int) {
            auto ret = *this;
            --(*this);
            return ret;
        }

        constexpr span_iterator operator+(difference_type n) const {
            auto ret = *this;
            return ret += n;
        }

        constexpr span_iterator& operator+=(difference_type n) {
            expects((index_ + n) >= 0 and (index_ + n) <= span_->size());
            index_ += n;
            return *this;
        }

        constexpr span_iterator operator-(difference_type n) const {
            auto ret = *this;
            return ret -= n;
        }

        constexpr span_iterator& operator-=(difference_type n) { return *this += -n; }

        constexpr difference_type operator-(span_iterator rhs) const {
            expects(span_ == rhs.span_);
            return index_ - rhs.index_;
        }

        constexpr reference operator[](difference_type n) const {
            return *(*this + n);
        }

        constexpr friend bool operator==(span_iterator lhs,
                                         span_iterator rhs) noexcept {
            return lhs.span_ == rhs.span_ && lhs.index_ == rhs.index_;
        }

        constexpr friend bool operator!=(span_iterator lhs,
                                         span_iterator rhs) noexcept {
            return !(lhs == rhs);
        }

        constexpr friend bool operator<(span_iterator lhs,
                                        span_iterator rhs) noexcept {
            return lhs.index_ < rhs.index_;
        }

        constexpr friend bool operator<=(span_iterator lhs,
                                         span_iterator rhs) noexcept {
            return !(rhs < lhs);
        }

        constexpr friend bool operator>(span_iterator lhs,
                                        span_iterator rhs) noexcept {
            return rhs < lhs;
        }

        constexpr friend bool operator>=(span_iterator lhs,
                                         span_iterator rhs) noexcept {
            return !(rhs > lhs);
        }

    protected:
        const Span* span_ = nullptr;
        std::ptrdiff_t index_ = 0;
    };

    template <class Span, bool IsConst>
    constexpr span_iterator<Span, IsConst>
    operator+(typename span_iterator<Span, IsConst>::difference_type n,
              span_iterator<Span, IsConst> rhs) {
        return rhs + n;
    }

    template <class Span, bool IsConst>
    constexpr span_iterator<Span, IsConst>
    operator-(typename span_iterator<Span, IsConst>::difference_type n,
              span_iterator<Span, IsConst> rhs) {
        return rhs - n;
    }

    template <std::ptrdiff_t Ext> class extent_type {
    public:
        using index_type = std::ptrdiff_t;
        static_expects(Ext >= 0); // "A fixed-size span must be >= 0 in size."
        constexpr extent_type() noexcept {}
        template <index_type Other>
        constexpr extent_type(extent_type<Other> ext) {
            static_expects(Other == Ext or Other == dynamic_extent);
                          // "Mismatch between fixed-size extent and size of "
                          // "initializing data."
            expects(ext.size() == Ext);
        }
        constexpr extent_type(index_type size) { expects(size == Ext); }
        constexpr index_type size() const noexcept { return Ext; }
    };

    template <> class extent_type<dynamic_extent> {
    public:
        using index_type = std::ptrdiff_t;
        template <index_type Other>
        explicit constexpr extent_type(extent_type<Other> ext)
            : size_(ext.size()) {}
        explicit constexpr extent_type(index_type size) : size_(size) {
            expects(size >= 0);
        }
        constexpr index_type size() const noexcept { return size_; }
    private:
        index_type size_;
    };

    template <class ElementType, std::ptrdiff_t Extent, std::ptrdiff_t Offset,
              std::ptrdiff_t Count>
    struct calculate_subspan_type {
        using type =
            Span<ElementType,
                 Count != dynamic_extent
                     ? Count
                     : (Extent != dynamic_extent ? Extent - Offset : Extent)>;
    };
} // namespace details

    // [span], class template span
template <class ElementType, std::ptrdiff_t Extent> class Span {
public:
    // constants and types
    using element_type = ElementType;
    using value_type = std::remove_cv_t<ElementType>;
    using index_type = std::ptrdiff_t;
    using pointer = element_type *;
    using reference = element_type &;
    using iterator = details::span_iterator<Span<ElementType, Extent>, false>;
    using const_iterator =
        details::span_iterator<Span<ElementType, Extent>, true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using size_type = index_type;

    static constexpr index_type extent{Extent};

    // [span.cons], span constructors, copy, assignment, and destructor
    template <bool Dependent = false,
              // "Dependent" is needed to make "std::enable_if_t<Dependent ||
              // Extent <= 0>" SFINAE,
              // since "std::enable_if_t<Extent <= 0>" is ill-formed when Extent
              // is greater than 0.
              class = std::enable_if_t<(Dependent or Extent <= 0)>>
    constexpr Span() noexcept : storage_(nullptr, details::extent_type<0>()) {}

    constexpr Span(pointer ptr, index_type count) : storage_(ptr, count) {}

    constexpr Span(pointer firstElem, pointer lastElem)
        : storage_(firstElem, std::distance(firstElem, lastElem)) {}

    template <std::size_t N>
    constexpr Span(element_type (&arr)[N]) noexcept
        : storage_(KnownNotNull{&arr[0]}, details::extent_type<N>()) {}

    template <std::size_t N,
              class ArrayElementType = std::remove_const_t<element_type>>
    constexpr Span(std::array<ArrayElementType, N> &arr) noexcept
        : storage_(&arr[0], details::extent_type<N>()) {}

    template <std::size_t N>
    constexpr Span(
        const std::array<std::remove_const_t<element_type>, N> &arr) noexcept
        : storage_(&arr[0], details::extent_type<N>()) {}

    // NB: the SFINAE here uses .data() as a incomplete/imperfect proxy for the
    // requirement
    // on Container to be a contiguous sequence container.
    /*
    template <
        class Container,
        class = std::enable_if_t<
            not details::is_span<Container>::value and
            not details::is_std_array<Container>::value and
            std::is_convertible<typename Container::pointer, pointer>::value and
            std::is_convertible<
                typename Container::pointer,
                decltype(std::declval<Container>().data())>::value>>
    constexpr Span(Container &cont)
        : Span(cont.data(), narrow<index_type>(cont.size())) {}

    template <
        class Container,
        class = std::enable_if_t<
            std::is_const<element_type>::value and
            not details::is_span<Container>::value and
            std::is_convertible<typename Container::pointer, pointer>::value and
            std::is_convertible<
                typename Container::pointer,
                decltype(std::declval<Container>().data())>::value>>
    constexpr Span(const Container &cont)
        : Span(cont.data(), narrow<index_type>(cont.size())) {}
    */
    constexpr Span(const Span &other) noexcept = default;

    template <
        class OtherElementType, std::ptrdiff_t OtherExtent,
        class = std::enable_if_t<
            details::is_allowed_extent_conversion<OtherExtent, Extent>::value and
            details::is_allowed_element_type_conversion<OtherElementType,
                                                        element_type>::value>>
    constexpr Span(const Span<OtherElementType, OtherExtent> &other)
        : storage_(other.data(),
                   details::extent_type<OtherExtent>(other.size())) {}

    ~Span() noexcept = default;
    constexpr Span &operator=(const Span &other) noexcept = default;

    // [span.sub], span subviews
    template <std::ptrdiff_t Count>
    constexpr Span<element_type, Count> first() const {
        expects(Count >= 0 and Count <= size());
        return {data(), Count};
    }

    template <std::ptrdiff_t Count>
    constexpr Span<element_type, Count> last() const {
        expects(Count >= 0 and size() - Count >= 0);
        return {data() + (size() - Count), Count};
    }

    template <std::ptrdiff_t Offset, std::ptrdiff_t Count = dynamic_extent>
    constexpr auto subspan() const ->
        typename details::calculate_subspan_type<ElementType, Extent, Offset,
                                                 Count>::type {
        expects((Offset >= 0 and size() - Offset >= 0) and
                (Count == dynamic_extent or
                 (Count >= 0 and Offset + Count <= size())));
        return {data() + Offset,
                Count == dynamic_extent ? size() - Offset : Count};
    }

    constexpr Span<element_type, dynamic_extent> first(index_type count) const {
        expects(count >= 0 and count <= size());
        return {data(), count};
    }
    constexpr Span<element_type, dynamic_extent> last(index_type count) const {
        return make_subspan(size() - count, dynamic_extent,
                            subspan_selector<Extent>{});
    }

    constexpr Span<element_type, dynamic_extent>
    subspan(index_type offset, index_type count = dynamic_extent) const {
        return make_subspan(offset, count, subspan_selector<Extent>{});
    }

    // [span.obs], span observers
    constexpr index_type size() const noexcept { return storage_.size(); }
    //constexpr index_type size_bytes() const noexcept {
    //    return size() * narrow_cast<index_type>(sizeof(element_type));
    //}
    constexpr bool empty() const noexcept { return size() == 0; }

    // [span.elem], span element access
    constexpr reference operator[](index_type idx) const {
        expects(idx >= 0 and idx < storage_.size());
        return data()[idx];
    }

    constexpr reference at(index_type idx) const {
        return this->operator[](idx);
    }
    constexpr reference operator()(index_type idx) const {
        return this->operator[](idx);
    }
    constexpr pointer data() const noexcept { return storage_.data(); }

    // [span.iter], span iterator support
    constexpr iterator begin() const noexcept { return {this, 0}; }
    constexpr iterator end() const noexcept { return {this, size()}; }

    constexpr const_iterator cbegin() const noexcept { return {this, 0}; }
    constexpr const_iterator cend() const noexcept { return {this, size()}; }

    constexpr reverse_iterator rbegin() const noexcept {
        return reverse_iterator{end()};
    }
    constexpr reverse_iterator rend() const noexcept {
        return reverse_iterator{begin()};
    }

    constexpr const_reverse_iterator crbegin() const noexcept {
        return const_reverse_iterator{cend()};
    }
    constexpr const_reverse_iterator crend() const noexcept {
        return const_reverse_iterator{cbegin()};
    }

private:
    // Needed to remove unnecessary null check in subspans
    struct KnownNotNull {
        pointer p;
    };

    // this implementation detail class lets us take advantage of the
    // empty base class optimization to pay for only storage of a single
    // pointer in the case of fixed-size spans
    template <class ExtentType> class storage_type : public ExtentType {
    public:
        // KnownNotNull parameter is needed to remove unnecessary null check
        // in subspans and constructors from arrays
        template <class OtherExtentType>
        constexpr storage_type(KnownNotNull data, OtherExtentType ext)
            : ExtentType(ext), data_(data.p) {
            expects(ExtentType::size() >= 0);
        }
        template <class OtherExtentType>
        constexpr storage_type(pointer data, OtherExtentType ext)
            : ExtentType(ext), data_(data) {
            expects(ExtentType::size() >= 0);
            expects(data or ExtentType::size() == 0);
        }
        constexpr pointer data() const noexcept { return data_; }
    private:
        pointer data_;
    };

    storage_type<details::extent_type<Extent>> storage_;

    // The rest is needed to remove unnecessary null check
    // in subspans and constructors from arrays
    constexpr Span(KnownNotNull ptr, index_type count) : storage_(ptr, count) {}

    template <std::ptrdiff_t CallerExtent> class subspan_selector {};

    template <std::ptrdiff_t CallerExtent>
    Span<element_type, dynamic_extent>
    make_subspan(index_type offset, index_type count,
                 subspan_selector<CallerExtent>) const {
        Span<element_type, dynamic_extent> tmp(*this);
        return tmp.subspan(offset, count);
    }

    Span<element_type, dynamic_extent>
    make_subspan(index_type offset, index_type count,
                 subspan_selector<dynamic_extent>) const {
        expects(offset >= 0 and size() - offset >= 0);
        if (count == dynamic_extent) {
            return {KnownNotNull{data() + offset}, size() - offset};
        }
        expects(count >= 0 and size() - offset >= count);
        return {KnownNotNull{data() + offset}, count};
    }
};

// [span.comparison], span comparison operators
template <class ElementType, std::ptrdiff_t FirstExtent,
          std::ptrdiff_t SecondExtent>
constexpr bool operator==(Span<ElementType, FirstExtent> l,
                          Span<ElementType, SecondExtent> r) {
    return std::equal(l.begin(), l.end(), r.begin(), r.end());
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr bool operator!=(Span<ElementType, Extent> l,
                          Span<ElementType, Extent> r) {
    return not(l == r);
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr bool operator<(Span<ElementType, Extent> l,
                         Span<ElementType, Extent> r) {
    return std::lexicographical_compare(l.begin(), l.end(), r.begin(), r.end());
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr bool operator<=(Span<ElementType, Extent> l,
                          Span<ElementType, Extent> r) {
    return not(l > r);
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr bool operator>(Span<ElementType, Extent> l,
                         Span<ElementType, Extent> r) {
    return r < l;
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr bool operator>=(Span<ElementType, Extent> l,
                          Span<ElementType, Extent> r) {
    return not(l < r);
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr typename Span<ElementType, Extent>::iterator
begin(Span<ElementType, Extent>& s) {
    return s.begin();
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr typename Span<ElementType, Extent>::const_iterator
begin(Span<ElementType, Extent> const& s) {
    return s.begin();
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr typename Span<ElementType, Extent>::iterator
end(Span<ElementType, Extent>& s) {
    return s.end();
}

template <class ElementType, std::ptrdiff_t Extent>
constexpr typename Span<ElementType, Extent>::const_iterator
end(Span<ElementType, Extent> const& s) {
    return s.end();
}

namespace details {
// if we only supported compilers with good constexpr support then this pair of
// classes could collapse down to a constexpr function we should use a
// narrow_cast<> to go to std::size_t, but older compilers may not see it as
// constexpr and so will fail compilation of the template.
template <class ElementType, std::ptrdiff_t Extent>
struct calculate_byte_size
    : std::integral_constant<std::ptrdiff_t,
                             static_cast<std::ptrdiff_t>(
                                 sizeof(ElementType) *
                                 static_cast<std::size_t>(Extent))> {};

template <class ElementType>
struct calculate_byte_size<ElementType, dynamic_extent>
    : std::integral_constant<std::ptrdiff_t, dynamic_extent> {};
}

// [span.objectrep], views of object representation
template <class ElementType, std::ptrdiff_t Extent>
Span<const unsigned char, details::calculate_byte_size<ElementType, Extent>::value>
as_bytes(Span<ElementType, Extent> s) noexcept {
    return {reinterpret_cast<const unsigned char *>(s.data()), s.size_bytes()};
}

template <class ElementType, std::ptrdiff_t Extent,
          class = std::enable_if_t<!std::is_const<ElementType>::value>>
Span<unsigned char, details::calculate_byte_size<ElementType, Extent>::value>
as_writeable_bytes(Span<ElementType, Extent> s) noexcept {
    return {reinterpret_cast<unsigned char *>(s.data()), s.size_bytes()};
}

//
// make_span() - Utility functions for creating spans
//
template <class ElementType>
constexpr Span<ElementType>
make_span(ElementType *ptr, typename Span<ElementType>::index_type count) {
    return Span<ElementType>(ptr, count);
}

template <class ElementType>
constexpr Span<ElementType> make_span(ElementType *firstElem,
                                      ElementType *lastElem) {
    return Span<ElementType>(firstElem, lastElem);
}

template <class ElementType, std::size_t N>
constexpr Span<ElementType, N> make_span(ElementType (&arr)[N]) noexcept {
    return Span<ElementType, N>(arr);
}

template <class Container>
constexpr Span<typename Container::value_type> make_span(Container &cont) {
    return Span<typename Container::value_type>(cont);
}

template <class Container>
constexpr Span<const typename Container::value_type>
make_span(const Container &cont) {
    return Span<const typename Container::value_type>(cont);
}

template <class Ptr>
constexpr Span<typename Ptr::element_type> make_span(Ptr &cont,
                                                     std::ptrdiff_t count) {
    return Span<typename Ptr::element_type>(cont, count);
}

template <class Ptr>
constexpr Span<typename Ptr::element_type> make_span(Ptr &cont) {
    return Span<typename Ptr::element_type>(cont);
}

} // namespace ec
#endif // GSL_SPAN_H
