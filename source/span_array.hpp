#ifndef ec_span_array_hpp
#define ec_span_array_hpp

#include <array>
#include <vector>
#include <algorithm>
#include "range.hpp"
#include "span.hpp"
#include "buffer.hpp"

namespace ec {

template<typename T, size_t N = 2>
struct Span_array {
    static const size_t Max_size = N;
    using Storage = std::array<Span<T>, Max_size>;
    
    Span_array()= default;
    Span_array(Span<T> &&s) {
        _spans[0] = std::move(s);
        _size = 1;
    }
    Span_array(Span<T> const &s) {
        _spans[0] = s;
        _size = 1;
    }
    Span_array(std::vector<Buffer<T>> & u) {
        _size = std::min(u.size(), 2ul);
        for (size_t i=0; i < _size; ++i) {
            _spans[i] = Span<T>(begin(u[i]), end(u[i]));
        }
    }
    template<typename U> Span_array(U const& u) {
        _spans[0] = Span<T>(begin(u), end(u));
        _size = 1;
    }
    Span_array(Span<T> &&s1, Span<T> &&s2) {
        _spans[0] = std::move(s1);
        _spans[1] = std::move(s2);
        _size = 2;
    }
    Span_array(Span<T> const &s1, Span<T> const &s2) {
        _spans[0] = s1;
        _spans[1] = s2;
        _size = 2;
    }
    template<typename U> Span_array(U && u1, U && u2) {
        _spans[0] = Span<T>(begin(u1), end(u1));
        _spans[1] = Span<T>(begin(u2), end(u2));
        expects(_spans[0].size() == _spans[1].size());
        _size = 2;
    }
    Span_array(Span_array const &) = default;
    Span_array &operator=(Span_array const &) = default;

    constexpr size_t span_size() const { return _spans[0].size(); }
    constexpr size_t size() const { return _size; }
    constexpr size_t max_size() const { return Max_size; }
    constexpr size_t frame_count() const { return _spans[0].size(); }
    constexpr Span<T> &operator[](Expects_within<size_t, 0, Max_size> n) {
        return _spans[n];
    }
    constexpr Span<T> const &operator[](Expects_within<size_t, 0, Max_size> n) const {
        return _spans[n];
    }
    
    template <typename F> constexpr void per_span(F f) {
        for (size_t i = 0; i < _size; ++i) {
            f(i, _spans[i]);
        }
    }

    Span_array subspans(Range<size_t> r) {
        auto copy = *this;
        copy.per_span(
            [=](size_t, auto &t) { t = t.subspan(from(r), r.size()); });
        return copy;
    }
    
    Span_array subspans(Range<size_t> r, size_t chans) {
        auto copy = subspans(r);
        copy._size = std::min(chans, copy._size);
        return copy;
    }
    
    Span_array channels(size_t chans) {
        auto copy = subspans({0ul, span_size()});
        copy._size = std::min(chans, copy._size);
        return copy;
    }

private:
    Storage _spans;
    size_t _size = 0; // 1...max_channels
};

} // namespace
#endif // ec_span_array_hpp
