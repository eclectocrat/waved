#ifndef _ec_view_hpp
#define _ec_view_hpp

#include "expects.hpp"
#include "range.hpp"
#include <memory>
#include <algorithm>

namespace ec {

template <typename T> struct View {
    constexpr View() = default;
    constexpr View(View const &) = default;
    View &operator=(View const &) = default;

    View(T * p, Range<size_t> r): _data(p), _range(r) {
        expects(p);
    }
    
    using Iterator = T*;
    using Const_iterator = T const*;
    
    Iterator        begin()         { return _data + from(_range); }
    Const_iterator  begin() const   { return _data + from(_range); }
    Iterator        end()           { return _data + to(_range); }
    Const_iterator  end() const     { return _data + to(_range); }

    constexpr T &operator[](size_t n) TESTEXCEPT {
        expects(n < _range.size());
        return *(_data + from(_range) + n);
    }

    constexpr T const &operator[](size_t n) const TESTEXCEPT {
        expects(n < _range.size());
        return *(_data + from(_range) + n);
    }

    constexpr T *ptr() { return _data ? _data + from(_range) : nullptr; }
    constexpr T const *ptr() const { return _data ? _data + from(_range) : nullptr; }
    constexpr size_t size() const { return _range.size(); }
    constexpr bool empty() const { return _range.size() == 0; }

    View subview(Range<size_t> r) const TESTEXCEPT {
        expects(_range.size() >= to(r));
        auto v = *this;
        v._range = offset(r, from(_range));
        return v;
    }

    View front(const size_t off) const TESTEXCEPT {
        expects(off < size());
        auto ret = subview({0, off});
        return ret;
    }

    View back(const size_t off) const TESTEXCEPT {
        expects(off < size());
        auto ret = subview({off, _range.size()});
        return ret;
    }

    void write(View const &view, const size_t loc) {
        expects(size() >= loc + view.size());
        std::memcpy(reinterpret_cast<void *>(_data + from(_range) + loc),
                    reinterpret_cast<void const *>(view._data),
                    view.size() * sizeof(T));
    }

private:
    T * _data;
    Range<size_t> _range;
};

template <typename T> constexpr typename View<T>::Iterator begin(View<T> &v) {
    return v.begin();
}
template <typename T> constexpr typename View<T>::Iterator end(View<T> &v) {
    return v.end();
}
template <typename T> constexpr typename View<T>::Const_iterator begin(
    View<T> const &v) {
    return v.begin();
}
template <typename T> constexpr typename View<T>::Const_iterator end(
    View<T> const &v) {
    return v.end();
}

} // namespace util
} // namespace ec
#endif // _ec_view_h
