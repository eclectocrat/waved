#ifndef ec_wave_hpp
#define ec_wave_hpp

#include "expects.hpp"
#include "range.hpp"
#include "span.hpp"
#include "span_array.hpp"
#include "buffer.hpp"
#include <list>
#include <functional>

namespace ec {

template<typename Sample_t>
struct Wave {
    using Sample        = Sample_t;
    using Range         = Range<size_t>;
    using Buffer        = Buffer<Sample>;
    using Buffer_list   = std::vector<Buffer>;
    using Span          = Span<Sample>;
    using Span_array    = Span_array<Sample, 2>;
    using Stream        = std::function<Span_array (Range, Span_array)>;

    Wave() = default;
    Wave(Wave const&) = default;
    Wave& operator=(Wave const&) = default;
    Wave(Stream const &stream, size_t channels, Range range) {
        set_stream(stream, channels, range);
    }
    Wave(Buffer_list const &samples) { set_map(samples); }
    Wave(Buffer_list const &samples, Stream const &stream, size_t channels,
         Range range) {
        set_stream(stream, channels, range);
        set_map(samples);
        expects(backings_are_synced());
    }
    explicit Wave(size_t n): _frame_offset(n) {} // for binary search

    bool is_backed() const { return not _samples.empty() or _stream != nullptr; }
    bool is_double_backed() const {
        return not _samples.empty() and _stream != nullptr;
    }
    bool backings_are_synced() const {
        return not is_backed()
            or (is_double_backed()
            and _samples.size() == _stream_channels
            and _samples.front().size() == _stream_range.size());
    }

    size_t channel_count() const {
        return not _samples.empty() ? _samples.size() : _stream_channels;
    }
    size_t frame_count() const {
        return not _samples.empty()
            ? _samples.front().size()
            : _stream_range.size();
    }

    size_t offset() const { return _frame_offset; }
    size_t next_offset() const { return offset() + frame_count(); }
    void set_offset(size_t n) { _frame_offset = n; }
    void move_offset(int n) { _frame_offset += n; }
    
    Range range() const { return {offset(), next_offset()}; }
    
    Buffer_list &samples() { return _samples; }
    Buffer_list const &samples() const { return _samples; }

    void set_stream(Stream const& stream, size_t channels, Range range) {
        expects(channels > 0);
        expects(channels <= 2);
        expects(not range.empty());
        _stream = stream;
        _stream_channels = channels;
        _stream_range = range;
    }

    void reset_stream() {
        _stream.reset();
        _stream_channels = 0;
        _stream_range = {};
    }

    void map_stream_frames(size_t stream_chunk_size) {
        expects(_stream != nullptr);
        expects(backings_are_synced());
        _assert_buffers();
        Span_array chans = _samples;
        for (size_t i=0; i < _stream_range.size(); ) {
            const auto j = i + std::min(stream_chunk_size / _stream_channels,
                                        _stream_range.size() - i);
            _stream({i + from(_stream_range),
                     j + from(_stream_range)}, chans.subspans({i, j}));
            i = j;
        }
    }

    void set_map(Buffer_list const& samples) {
        _samples = samples;
        _assert_buffers();
    }

    void unmap_frames() { _samples.clear(); }
    
    Span_array map() {
        return _samples;
    }

    Wave split_back(size_t n) {
        expects(n > 0); // can't split at beginning or I'd destroy myself
        expects(n <= frame_count());
        if (n == frame_count()) {
            return Wave(); // split back of zero length
        }
        Wave ret = *this;
        ret._frame_offset += n;
        if (_stream != nullptr) {
            ret._stream_range = _stream_range.slice(n, to(_stream_range));
            _stream_range = _stream_range.slice(from(_stream_range), n);
        }
        if (not _samples.empty()) {
            for (size_t c = 0; c < channel_count(); ++c) {
                ret._samples[c] = _samples[c].split_back(n);
            }
        }
        return ret;
    }

private:
    void _assert_buffers() {
        expects(_samples.empty()            // no channels or
            or (_samples.front().size() > 0 // at least 1 frame in
            and(_samples.size() == 1        // a mono list or
            or (_samples.size() == 2        // the same number of frames in a stereo list.
            and _samples.front().size() == _samples.back().size()))));
    }

    size_t _frame_offset = 0;

    // memory backing
    Buffer_list _samples;

    // stream backing
    Stream  _stream;
    Range   _stream_range;
    size_t  _stream_channels = 0;
};

} // namespace
#endif /* ec_wave_hpp */
