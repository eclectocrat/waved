#ifndef ec_wave_list_hpp
#define ec_wave_list_hpp

#include "expects.hpp"
#include "range.hpp"
#include "wave.hpp"
#include "for_chunks.hpp"
#include <list>

namespace ec {

template <typename Sample_t>
class Wave_list: public std::list<Wave<Sample_t>> {
public:
    using List      = std::list<Wave<Sample_t>>;
    using Iterator  = typename List::iterator;
    using Range     = Range<size_t>;
    using Wave      = Wave<Sample_t>;

    Wave_list() = default;
    Wave_list(Wave_list const &) = default;
    Wave_list &operator=(Wave_list const &) = default;
    Wave_list(Wave_list &&) = default;
    Wave_list &operator=(Wave_list &&) = default;

    size_t frame_count() const {
        return List::empty() ? 0 : List::back().next_offset();
    }
    size_t channel_count() const {
        return List::empty() ? 0 : List::front().channel_count();
    }
    
    template <typename F>
    void for_map_chunks(Range const &range, size_t chan, F func) {
        ec::for_chunks(*this, range,
                   [&](auto c) { return c.range(); },
                   [&](auto c) {
                        expects(c.channel_count() > chan);
                        expects(not c.map()[chan].empty());
                        return c.map()[chan];
                   }, func);
    }

    template <typename F>
    void for_map_samples(Range const &range, size_t chan, F func) {
        for_map_chunks(range, chan,
                  [&](auto, auto b, auto e) { std::for_each(b, e, func); });
    }

    /// Return values can be:
    ///  (begin, begin] => n = 0
    ///  (i, j]         => left clip, and right clip or end, i != j
    ///  (end, end]     => n = frame_count()
    std::pair<Iterator, Iterator> split_if_needed(size_t n) {
        expects(n <= frame_count());
        // extent; end
        auto b = wave_at_frame(n);
        if (b == List::end()) {
            return {List::end(), List::end()};
        }
        auto &left = *b;
        // pre-existing split | extent; begin
        if (n == left.offset()) {
            auto right = b;
            return {right != List::begin() ? --right : right, right};
        }
        // pre-existing split
        if (n == left.next_offset()) {
            auto e = b;
            return {b, ++e};
        }
        // make new split
        auto e = b;
        const auto right = left.split_back(n - left.offset());
        return {b, List::insert(++e, right)};
    }

    Iterator wave_at_frame(size_t n) {
        return std::lower_bound(List::begin(), List::end(), Wave(n),
                                [&](auto const &a, auto const &b) {
                                    return a.next_offset() < b.next_offset();
                                });
    }
    
    void calculate_wave_offsets(Iterator begin) {
        auto n = begin->offset();
        std::for_each(begin, List::end(), [&](auto &c) {
            c.set_offset(n);
            n += c.frame_count();
        });
    }
    
    void move_wave_offsets(Iterator begin, int n) {
        std::for_each(begin, List::end(), [&](auto &c) { c.move_offset(n); });
    }
};

} // namespace ec
#endif // ec_wave_list_hpp
