#ifndef ec_wave_list_funcs_hpp
#define ec_wave_list_funcs_hpp

#include "wave_list.hpp"
#include <algorithm>

namespace ec {


/// \brief Erases a range from a list.
///
/// \param from_list    The list to erase the range from.
/// \param from_range   The range to remove from the from_list. Must be within
///                     the bounds of (0, from_list.frame_count()).
template<typename Sample>
void erase(Wave_list<Sample>& from_list, Range<size_t> from_range) {
    auto l = from_list.split_if_needed(from(from_range));
    auto r = from_list.split_if_needed(to(from_range));
    from_list.erase(l.second, r.second);
    from_list.move_wave_offsets(++l.first, -int(from_range.size()));
}


/// \brief Moves a range of one list and into another list.
///
/// \param from_list    The list to move the range OUT of.
/// \param from_range   The range to move out of the from_list. Must be within
///                     the bounds of (0, from_list.frame_count()].
/// \param to_list      The list to move the range IN to.
/// \param to_pos       The offset at which to insert the moved range in
///                     to_list. Must be <= to_list.frame_count().
template<typename S>
void move(Wave_list<S>& from_list, Range<size_t> from_range,
          Wave_list<S>& to_list, size_t to_pos) {
    expects(to_pos <= to_list.frame_count());
    auto l = from_list.split_if_needed(from(from_range));
    auto r = from_list.split_if_needed(to(from_range));
    auto i = to_list.split_if_needed(to_pos);
    to_list.splice(i.second, from_list, l.second, r.second);
    to_list.calculate_wave_offsets(i.first);
    from_list.move_wave_offsets(r.second, -int(from_range.size()));
}


/// \brief Copies a range of one list to another.
///
/// \see move()
template<typename S>
void copy(Wave_list<S>& from_list, Range<size_t> from_range,
          Wave_list<S>& to_list, size_t to_pos) {
    Wave_list<S> copy = from_list;
    move(copy, from_range, to_list, to_pos);
}


/// \brief Reads a range of a lists mapped samples.
template<typename S>
void read_map(Wave_list<S>& from_list, Range<size_t> from_range,
              size_t from_chan, Span<S> to_span) {
    expects(to_span.size() >= from_range.size());
    expects(to(from_range) <= from_list.frame_count());
    from_list.for_map_chunks(from_range, from_chan,
        [&](auto rng, auto b, auto e) {
            std::copy(b, e, begin(to_span) + (from(rng) - from(from_range)));
        });
}


template<typename S>
void read_stream(Wave_list<S> const& from_list, Range<size_t> from_range,
                 size_t from_chan, Span<S> to_span) {
    expects(to_span.size() >= from_range.size());
    expects(to(from_range) <= from_list.frame_count());
    // ?
}


//template<typename S>
//void insert_uninitialized(Wave_list<S>& to_list, size_t to_pos, size_t amt, size_t channels = 0) {
//    expects(to_pos <= to_list.frame_count());
//    if (auto chans = to_list.channel_count(); chans != 0) {
//        channels = channels == 0 ? chans : channels;
//        expects(channels == to_list.channel_count());
//    } else {
//        expects(channels > 0);
//        expects(channels <= 2);
//    }
//    if(amt < 1) {
//        return;
//    }
//    auto c = Clip();
//    c._samples.resize(channels);
//    for(auto &s: c._samples) {
//        s = Buffer(new Sample[amt], amt, [](auto p, auto) { delete[] p; });
//    }
//    auto i = split_if_needed(pos);
//    _clips.insert(i.second, c);
//    update_clip_offsets(i.first);
//}

} // namespace ec
#endif /* ec_wave_list_funcs_hpp */
