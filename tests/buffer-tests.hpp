#ifndef BUFFERTESTS_HPP
#define BUFFERTESTS_HPP

#include "buffer.hpp"

namespace ec {
namespace test {
using namespace std;
typedef Buffer<int> Buffer_t;

TEST_CASE("[buffer<T>] VALID construction") {
    SUBCASE("[buffer<T>] default construction") {
        Buffer_t buff;
        CHECK(buff.empty());
        CHECK(buff.size() == 0);
        CHECK(buff.data() == nullptr);
    } SUBCASE("[buffer<T>] size") {
        Buffer_t buff(new int[32], 32, [](auto s, auto) { delete [] s; });
        CHECK(not buff.empty());
        CHECK(buff.size() == 32);
    } SUBCASE("[buffer<T>] memory") {
        int raw[32];
        Buffer_t buff(raw, 32, [](auto, auto){});
        CHECK(buff.size() == 32);
        CHECK(buff.data() == raw);
    }
}
TEST_CASE("[buffer<T>] INVALID construction") {
    REQUIRE_THROWS(Buffer_t(nullptr, 1, [](auto, auto){}));
    REQUIRE_THROWS(Buffer_t(reinterpret_cast<int *>(1), 0, [](auto, auto){}));
}
TEST_CASE("[buffer<T>] VALID slice") {}
TEST_CASE("[buffer<T>] INVALID slice") {}
TEST_CASE("[buffer<T>] VALID write") {}
TEST_CASE("[buffer<T>] INVALID write") {}

} // namespace test
} // namespace ec
#endif // BUFFERTESTS_HPP
