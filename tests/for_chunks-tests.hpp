#ifndef FOR_CHUNKSTESTS_HPP
#define FOR_CHUNKSTESTS_HPP

#include <array>
#include "for_chunks.hpp"

namespace ec {
namespace test {
using namespace std;
struct Node {
    Node() = default;
    Node(size_t n): off(n) {}
    size_t off = 0;
    size_t len = 0;
    vector<int> vec;
};
TEST_CASE("for_chunks() empty") {
    array<Node, 0> empty;
    SUBCASE("empty list and/or empty range") {
        REQUIRE_THROWS(for_chunks(
            empty, Range(), [](auto) { return Range(); },
            [](auto &n) -> auto & { return n.vec; },
            [&](auto, auto b, auto e) {}));
    } SUBCASE("empty node") {
        array<Node, 1> one;
        REQUIRE_THROWS(for_chunks(
            one, Range(0ul, 1ul), [](auto) { return Range(); },
            [](auto &n) -> auto & { return n.vec; },
            [&](auto, auto b, auto e) {}));
    }
}
TEST_CASE("for_chunks() single node") {
    array<Node, 1> one;
    SUBCASE("visits all elements once") {
        one.front().vec.resize(9, 0);
        one.front().len = 9;
        for_chunks(
            one, Range(0ul, 9ul),
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [](auto, auto b, auto e) { for_each(b, e, [](auto &i) { i += 1; }); });
        CHECK(find_if(begin(one.front().vec), end(one.front().vec),
                      [](auto i) { return i != 1; }) == end(one.front().vec));
    } SUBCASE("visits a subrange of elements once and skips others") {
        one.front().vec.resize(9, 0);
        one.front().len = 9;
        for_chunks(
            one, Range(5ul, 9ul),
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [](auto, auto b, auto e) { for_each(b, e, [](auto &i) { i += 1; });
            });
        CHECK(find_if(begin(one.front().vec), begin(one.front().vec) + 5,
                      [](auto i) { return i != 0; }) ==
              begin(one.front().vec) + 5);
        CHECK(find_if(begin(one.front().vec) + 5, end(one.front().vec),
                      [](auto i) { return i != 1; }) == end(one.front().vec));
    } SUBCASE("gives expected Range param") {
        one.front().vec.resize(9, 0);
        one.front().len = 9;
        const auto range = Range(5ul, 9ul);
        for_chunks(
            one, range,
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [=](auto r, auto, auto) { CHECK(range == r); });
    }
}
TEST_CASE("for_chunks() multi-node") {
    array<Node, 3> multi;
    size_t chunk_size = 7;
    size_t chunk_off = 0;
    for_each(begin(multi), end(multi), [&](auto& n) {
        n.vec.resize(chunk_size);
        n.off = chunk_off;
        n.len = chunk_size;
        chunk_off += chunk_size;
        chunk_size += 3;
    });
    const size_t total_size =
        accumulate(begin(multi), end(multi), 0,
                   [](auto t, auto &n) { return t + n.vec.size(); });
    expects(total_size == 30); // assertion for test code, not an actual test
    expects(multi[0].off == 0 and multi[0].len == 7);
    expects(multi[1].off == 7 and multi[1].len == 10);
    expects(multi[2].off == 17 and multi[2].len == 13);
    SUBCASE("throws out of bounds") {
        REQUIRE_THROWS(for_chunks(
            multi, Range(31ul, 32ul), [](auto &n) { return Range(); },
            [](auto &n) -> auto & { return n.vec; }, [](auto, auto, auto) {}));
    } SUBCASE("visits all elements once") {
        for_chunks(
            multi, Range(0ul, total_size),
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [](auto, auto b, auto e) { for_each(b, e, [](auto &i) { i += 1; }); });
        for_each(begin(multi), end(multi), [&](auto &n) {
            CHECK(find_if(begin(n.vec), end(n.vec),
                          [](auto i) { return i != 1; }) == end(n.vec));
        });
    } SUBCASE("visits a range of elements once and ignores rest") {
        for_chunks(
            multi, Range(4ul, total_size-4), // span all nodes
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [](auto, auto b, auto e) { for_each(b, e, [](auto &i) { i += 1; }); });
        CHECK(find_if(begin(multi[0].vec) + 4, end(multi[0].vec),
                      [](auto i) { return i != 1; }) == end(multi[0].vec));
        CHECK(find_if(begin(multi[1].vec), end(multi[1].vec),
                      [](auto i) { return i != 1; }) == end(multi[1].vec));
        CHECK(find_if(begin(multi[2].vec), end(multi[2].vec) - 4,
                      [](auto i) { return i != 1; }) == end(multi[2].vec) - 4);
        CHECK(find_if(begin(multi[2].vec), begin(multi[2].vec) + 4, [](auto i) {
                  return i != 1;
              }) == begin(multi[2].vec) + 4);
        CHECK(find_if(end(multi[2].vec) - 4, end(multi[2].vec),
                      [](auto i) { return i != 0; }) == end(multi[2].vec));
    } SUBCASE("gives expected Range params") {
        const auto range = Range(5ul, 19ul);
        vector<Range<size_t>> ranges;
        ranges.reserve(3);
        for_chunks(
            multi, range,
            [](auto &n) { return Range(n.off, n.off + n.len); },
            [](auto &n) -> auto & { return n.vec; },
            [&](auto r, auto, auto) { ranges.push_back(r); });
        CHECK(ranges[0] == Range(5ul, 7ul));
        CHECK(ranges[1] == Range(7ul, 17ul));
        CHECK(ranges[2] == Range(17ul, 19ul));
    }
}
} // namespace test
} // namespace ec
#endif // FOR_CHUNKSTESTS_HPP
