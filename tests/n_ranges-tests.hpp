#ifndef n_ranges_tests_h
#define n_ranges_tests_h

#include "n_ranges.hpp"
#include <vector>
#include <numeric>
#include <algorithm>

namespace test {
using namespace std;

TEST_CASE("[n_ranges] positive_ratio()") {
    SUBCASE("ratio < 1") {
        auto r = positive_ratio(10u, 100u);
        CHECK(r.first == 1u);
        CHECK(r.second == 10u);
        r = positive_ratio(2u, 5u);
        CHECK(r.first == 2u);
        CHECK(r.second == 5u);
    } SUBCASE("ratio >= 1") {
        auto r = positive_ratio(100u, 10u);
        CHECK(r.first == 10u);
        CHECK(r.second == 1u);
        r = positive_ratio(5u, 2u);
        CHECK(r.first == 5u);
        CHECK(r.second == 2u);
    } SUBCASE("with zero values") {
        auto r = positive_ratio(0u, 1u);
        CHECK(r.first == 0);
        CHECK(r.second == 1u);
        r = positive_ratio(1u, 0u);
        CHECK(r.first == 1u);
        CHECK(r.second == 0);
    } SUBCASE("contract violated") {
        REQUIRE_THROWS(positive_ratio(0u, 0u));
    }
}
} // namespace test

namespace test {
using namespace std;
typedef Range<size_t> Range_t;

TEST_CASE("[n_ranges_location_of_range] elements > ranges") {
    auto f = n_ranges_location_of_range<size_t>;
    SUBCASE("with remainder") {
        CHECK(f(13, 3, 0) == 0);
        CHECK(f(13, 3, 1) == 5);
        CHECK(f(13, 3, 2) == 9);
    } SUBCASE("without remainder") {
        CHECK(f(12, 3, 0) == 0);
        CHECK(f(12, 3, 1) == 4);
        CHECK(f(12, 3, 2) == 8);
    }
}
} // namespace test

namespace test {
using namespace std;

TEST_CASE("[N_ranges] default constructor") {
    N_ranges<size_t> ranges;
    CHECK(ranges.elements_size() == 0);
    CHECK(ranges.ranges_size() == 0);
    CHECK(ranges.elements_per_range() == 0);
    CHECK(ranges.range_min_size() == 0);
    CHECK(ranges.range_max_size() < 2);
}
 
TEST_CASE("[N_ranges] elements > ranges") {
    SUBCASE("WITH remainder") {
        N_ranges<size_t> ranges(13, 3);
        SUBCASE("elements and ranges are correct") {
            CHECK(ranges.elements_size() == 13);
            CHECK(ranges.ranges_size() == 3);
            CHECK(ranges.range_min_size() == 4);
            CHECK(ranges.range_max_size() == 5);
        }
    } SUBCASE("WITHOUT remainder") {
        N_ranges<size_t> ranges(12, 3);
        SUBCASE("elements and ranges are correct") {
            CHECK(ranges.elements_size() == 12);
            CHECK(ranges.ranges_size() == 3);
            CHECK(ranges.range_min_size() == ranges.range_max_size());
        }
    }
}

TEST_CASE("[n_ranges_generator] elements == ranges") {
    N_ranges<size_t> ranges(10, 10);
    CHECK(ranges.elements_size() == ranges.ranges_size());
    CHECK(ranges.range_max_size() == ranges.range_min_size());
}

TEST_CASE("[n_ranges_generator] elements < ranges") {
    N_ranges<size_t> ranges(3, 13);
    SUBCASE("elements and ranges are correct") {
        CHECK(ranges.elements_size() == 3);
        CHECK(ranges.ranges_size() == 13);
        CHECK(ranges.range_min_size() == 0);
        CHECK(ranges.range_max_size() == 1);
    }
}
} // namespace test

#endif /* n_ranges_tests_h */
