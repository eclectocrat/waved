#ifndef n_ranges_for_tests_h
#define n_ranges_for_tests_h

namespace test {
using namespace std;
TEST_CASE("[n_ranges] for_n_ranges_compress") {
    typedef N_ranges<size_t> N_ranges_t;
    typedef Range<size_t> Range_t;
    SUBCASE("rejects ranges.elements_size() <= ranges.ranges_size()") {
        REQUIRE_THROWS(for_n_ranges_compress(N_ranges_t(1, 2), Range_t(),
                                             [](auto, auto) {}));
        REQUIRE_THROWS(for_n_ranges_compress(N_ranges_t(1, 1), Range_t(),
                                             [](auto, auto) {}));
    } SUBCASE("visits all ranges once") {
        N_ranges_t ranges(15, 4);
        vector<int> visited(ranges.ranges_size());
        SUBCASE("piecemeal") {
            for_n_ranges_compress(ranges, Range_t(1),
                                  [&](auto i, auto r) { ++visited[i]; });
            CHECK(visited[0] == 1);
            CHECK(visited[1] == 0);
            CHECK(visited[2] == 0);
            CHECK(visited[3] == 0);
            for_n_ranges_compress(ranges, Range_t(3, 4),
                                  [&](auto i, auto r) { ++visited[i]; });
            CHECK(visited[0] == 1);
            CHECK(visited[1] == 0);
            CHECK(visited[2] == 0);
            CHECK(visited[3] == 1);
            for_n_ranges_compress(ranges, Range_t(1, 3),
                                  [&](auto i, auto r) { ++visited[i]; });
            CHECK(visited[0] == 1);
            CHECK(visited[1] == 1);
            CHECK(visited[2] == 1);
            CHECK(visited[3] == 1);
        } SUBCASE("all at once") {
            for_n_ranges_compress(ranges, Range_t(0, ranges.ranges_size()),
                                [&](auto i, auto r) { ++visited[i]; });
            CHECK(accumulate(
                begin(visited), end(visited), 1,
                [&](auto a, auto b) -> bool { return a == b == 1; }));
        }
    } SUBCASE("visits all elements once") {
        N_ranges_t ranges(17, 2);
        vector<int> visited(ranges.elements_size());
        SUBCASE("piecemeal") {
            for_n_ranges_compress(ranges, Range_t(0, 1), [&](auto i, auto r) {
                fill(begin(visited) + from(r), begin(visited) + to(r), 1);
            });
            CHECK(accumulate(
                begin(visited), begin(visited) + ranges.location_of_range(1), 1,
                [&](auto a, auto b) -> bool { return a == b == 1; }));
            for_n_ranges_compress(ranges, Range_t(1, 2), [&](auto i, auto r) {
                fill(begin(visited) + from(r), begin(visited) + to(r), 1);
            });
            CHECK(accumulate(
                begin(visited) + ranges.location_of_range(1), end(visited), 1,
                [&](auto a, auto b) -> bool { return a == b == 1; }));
        } SUBCASE("all at once") {
            for_n_ranges_compress(ranges, Range_t(0, ranges.ranges_size()),
                                  [&](auto i, auto r) {
                                      fill(begin(visited) + from(r),
                                           begin(visited) + to(r), 1);
                                  });
            CHECK(accumulate(
                begin(visited), end(visited), 1,
                [&](auto a, auto b) -> bool { return a == b == 1; }));
        }
    }
}
} // namespace test

namespace test {
using namespace std;
template<typename B, typename E>
void incr_each(B b, E e) {
    for_each(b, e, [](auto& v) { ++v; });
}
TEST_CASE("[n_ranges] for_n_ranges_expand") {
    typedef N_ranges<size_t> N_ranges_t;
    typedef Range<size_t> Range_t;
    SUBCASE("rejects ranges.elements_size() >= ranges.ranges_size()") {
        REQUIRE_THROWS(for_n_ranges_expand(N_ranges_t(2, 1), Range_t(),
                                           [](auto, auto) {}));
        REQUIRE_THROWS(for_n_ranges_expand(N_ranges_t(2, 2), Range_t(),
                                           [](auto, auto) {}));
    } SUBCASE("visits all ranges once") {
        N_ranges_t ranges(2, 4);
        vector<int> visited(ranges.ranges_size());
        SUBCASE("piecemeal") {
            for_n_ranges_expand(ranges, Range_t(1),
                                [&](auto i, auto r) { ++visited[i]; });
            CHECK(visited[0] == 1);
            CHECK(visited[1] == 0);
            CHECK(visited[2] == 0);
            CHECK(visited[3] == 0);
            for_n_ranges_expand(ranges, Range_t(3, 4),
                                [&](auto i, auto r) { ++visited[i]; });
            CHECK(visited[0] == 1);
            CHECK(visited[1] == 0);
            CHECK(visited[2] == 0);
            CHECK(visited[3] == 1);
            for_n_ranges_expand(ranges, Range_t(1, 3),
                                [&](auto i, auto r) { ++visited[i]; });
            CHECK(visited[0] == 1);
            CHECK(visited[1] == 1);
            CHECK(visited[2] == 1);
            CHECK(visited[3] == 1);
        } SUBCASE("all at once") {
            for_n_ranges_expand(ranges, Range_t(0, ranges.ranges_size()),
                                [&](auto i, auto r) { ++visited[i]; });
            CHECK(accumulate(
                begin(visited), end(visited), 1,
                [&](auto a, auto b) -> bool { return a == b == 1; }));
        }
    } SUBCASE("visits all elements once") {
        N_ranges_t ranges(5, 8);
        vector<int> visited(ranges.elements_size());
        SUBCASE("piecemeal") {
            for_n_ranges_expand(ranges, Range_t(0, 3), [&](auto i, auto r) {
                incr_each(begin(visited) + from(r), begin(visited) + to(r));
            });
            CHECK(accumulate(begin(visited),
                             begin(visited) + ranges.location_of_range(1), true,
                             [&](auto a, auto b) -> bool { return a and b; }));
            for_n_ranges_expand(ranges, Range_t(3, 8), [&](auto i, auto r) {
                incr_each(begin(visited) + from(r), begin(visited) + to(r));
            });
            CHECK(accumulate(
                begin(visited) + ranges.location_of_range(1), end(visited),
                1, [&](auto a, auto b) -> bool { return a == b == 1; }));
        } SUBCASE("all at once") {
            for_n_ranges_expand(ranges, Range_t(0, ranges.ranges_size()),
                                [&](auto i, auto r) {
                                    incr_each(begin(visited) + from(r),
                                              begin(visited) + to(r));
                                });
            CHECK(accumulate(
                begin(visited), end(visited), 1,
                [&](auto a, auto b) -> bool { return a == b == 1; }));
        } SUBCASE("each element belongs to only one range") {
            vector<int> visited_count(ranges.elements_size());
            for_n_ranges_expand(
                ranges, Range_t(0, ranges.ranges_size()), [&](auto i, auto r) {
                    for (size_t i = from(r); i != to(r); ++i) {
                        ++visited_count[i];
                    }
                });
            CHECK(find_if(begin(visited_count), end(visited_count),
                          [](auto n) -> bool { return n != 1; }) ==
                  end(visited_count));
        }
    }
}
} // namespace test
namespace test {
using namespace std;
TEST_CASE("[n_ranges] for_n_ranges_identity") {
    typedef N_ranges<size_t> N_ranges_t;
    typedef Range<size_t> Range_t;
    SUBCASE("rejects ranges.elements_size() != ranges.ranges_size()") {
        REQUIRE_THROWS(for_n_ranges_identity(N_ranges_t(2, 1), Range_t(),
                                             [](auto, auto) {}));
        REQUIRE_THROWS(for_n_ranges_identity(N_ranges_t(1, 2), Range_t(),
                                             [](auto, auto) {}));
    }
    /// @see n_ranges_identity_visit() implementation.
}
} // namespace test

#endif /* n_ranges_for_tests_h */
