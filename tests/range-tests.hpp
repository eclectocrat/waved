#ifndef RANGETESTS_HPP
#define RANGETESTS_HPP

#include "range.hpp"

namespace ec {
namespace test {
using namespace std;
typedef Range<size_t> Range_t;

TEST_CASE("[range<T>] VALID construction") {
    SUBCASE("[range<T>] default construction") {
        Range_t r;
        CHECK(r.from() == 0);
        CHECK(r.to() == 0);
        CHECK(r.size() == 0);
        CHECK(r.empty() == true);
    }
    SUBCASE("[range<T>] VALID index param construction") {
        Range_t r(0, 1);
        CHECK(r.from() == 0);
        CHECK(r.to() == 1);
        CHECK(r.size() == 1);
        CHECK(r.empty() == false);
    }
}
TEST_CASE("[range<T>] INVALID construction") {
    REQUIRE_THROWS(Range_t(10, 9));
}
TEST_CASE("[range<T>] VALID slice") {
    Range_t r(10, 20);
    auto sub = r.slice(5, 10);
    CHECK(sub.size() == 5);
    CHECK(sub.from() == 15);
    CHECK(sub.to() == 20);
}
TEST_CASE("[range<T>] INVALID slice") {
    Range_t r(10, 20);
    REQUIRE_THROWS(r.slice(5, 15));
}
TEST_CASE("[range<T>] contains_range") {
    Range_t container(10, 20);
    Range_t inside(11, 12);
    Range_t edge(19, 20);
    Range_t outside;
    Range_t overlapping(15, 25);

    CHECK(container.contains(inside) == true);
    CHECK(container.contains(edge) == true);
    CHECK(container.contains(container) == true);
    CHECK(container.contains(outside) == false);
    CHECK(container.contains(overlapping) == false);
}
TEST_CASE("[range<T>] operator <") {
    Range_t a(0, 1);
    Range_t b(1, 2);
    Range_t c(2, 3);
    Range_t d(3, 4);

    vector<Range_t> ranges{ a, c, d, b };
    sort(ranges.begin(), ranges.end());

    SUBCASE("[range<T>] operator < true") {
        CHECK(ranges[0] < ranges[1]);
        CHECK(ranges[1] < ranges[2]);
        CHECK(ranges[2] < ranges[3]);
    }
    SUBCASE("[range<T>] operator < false") {
        CHECK((ranges[1] < ranges[0]) == false);
        CHECK((ranges[2] < ranges[1]) == false);
        CHECK((ranges[3] < ranges[2]) == false);
    }
}
} // namespace test
} // namespace ec
#endif // RANGETESTS_HPP
