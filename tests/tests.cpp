#define DOCTEST_CONFIG_COLORS_NONE
#define DOCTEST_CONFIG_IMPLEMENT
#define TESTEXCEPT
#include "doctest.h"
// register tests here!
#include "buffer-tests.hpp"
#include "for_chunks-tests.hpp"
#include "range-tests.hpp"
#include "wave_list-tests.hpp"
#include "wave_list_funcs-tests.hpp"

int main(int argc, char **argv) {
    doctest::Context context;
    context.setOption("no-breaks", true);
    context.applyCommandLine(argc, argv);
    context.setOption("abort-after", 50);
    context.setOption("sort", "name");
    const int test_return_code = context.run();
    if (context.shouldExit()) {
        return test_return_code;
    }
    return test_return_code;
}
