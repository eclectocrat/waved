#ifndef WAVE_LISTTESTS_HPP
#define WAVE_LISTTESTS_HPP

#include "wave_list.hpp"

namespace ec {
namespace test {
TEST_CASE("[Wave_list<T>]") {
    SUBCASE("Empty list") {
        auto wlist = make_uninitialized_map<int>(1000);
        auto &wave = wlist.front();
        auto map = wave.map();
        std::iota(begin(map[0]), end(map[0]), 0);
    } SUBCASE("Empty range") {

    } SUBCASE("Empty list and empty range") {

    }
}
} // namespace test
} // namespace ec
#endif // WAVE_LISTTESTS_HPP
