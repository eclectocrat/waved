#ifndef WAVE_LIST_FUNCSTESTS_HPP
#define WAVE_LIST_FUNCSTESTS_HPP

#include "wave_list_funcs.hpp"

namespace ec {
namespace test {
// erase tests
TEST_CASE("[Wave_list erase] empty list") {
    Wave_list<int> list;
    erase(list, {});
    REQUIRE_THROWS(erase(list, {0, 1}));
} TEST_CASE("[Wave_list erase] empty range") {
    auto list = make_uninitialized_map<int>(1);
    erase(list, {});
    CHECK(list.frame_count() == 1);
} TEST_CASE("[Wave_list erase] everything") {
    auto list = make_uninitialized_map<int>(1);
    erase(list, {0, 1});
    CHECK(list.frame_count() == 0);
    CHECK(list.size() == 0);
} TEST_CASE("[Wave_list erase] front") {
    auto list = make_uninitialized_map<int>(2);
    erase(list, {0, 1});
    CHECK(list.frame_count() == 1);
    CHECK(list.size() == 1);
} TEST_CASE("[Wave_list erase] back") {
    auto list = make_uninitialized_map<int>(2);
    erase(list, {1, 2});
    CHECK(list.frame_count() == 1);
    CHECK(list.size() == 1);
} TEST_CASE("[Wave_list erase] middle") {
    auto list = make_uninitialized_map<int>(3);
    erase(list, {1, 2});
    CHECK(list.frame_count() == 2);
    CHECK(list.size() == 2);
} TEST_CASE("[Wave_list erase] multiple waves") {
    auto list = make_uninitialized_map<int>(4);
    list.split_if_needed(1);
    list.split_if_needed(2);
    list.split_if_needed(3);
    CHECK(list.size() == 4);
    erase(list, {1, 3});
    CHECK(list.frame_count() == 2);
    CHECK(list.size() == 2);
}
//TEST_CASE("[Wave_list erase] offsets are correct") {
    // TODO...
//}

// read_map tests
TEST_CASE("[Wave_list read_map]") {
    size_t size = 100;
    auto list = make_uninitialized_map<int>(size);
    auto map = list.front().map()[0];
    std::iota(begin(map), end(map), 1);
    int buff[size];
    SUBCASE("does nothing with empty range") {
        read_map(list, {}, 0, {buff, buff});
        CHECK(map[0] == 1);
        CHECK(map[1] == 2);
        CHECK(map[2] == 3);
    } SUBCASE("rejects range not in {0, list.frame_count()}") {
        REQUIRE_THROWS(read_map(list, {99, 101}, 0, {buff, buff+2}));
    } SUBCASE("rejects span.size() < range.size()") {
        REQUIRE_THROWS(read_map(list, {0, 2}, 0, {buff, buff+1}));
    } SUBCASE("rejects channel >= list.channel_count()") {
        REQUIRE_THROWS(read_map(list, {0, 1}, 1, {buff, buff+1}));
    } SUBCASE("reads expected map data") {
        auto expected = std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        read_map(list, {0, 10}, 0, {buff, buff+10});
        CHECK(std::equal(buff, buff+10, begin(expected)));
        expected = std::vector<int>{11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        read_map(list, {10, 20}, 0, {buff, buff+10});
        CHECK(std::equal(buff, buff+10, begin(expected)));
    }
}
} // namespace test
} // namespace ec
#endif // WAVE_LIST_FUNCSTESTS_HPP
